/*! QiscusTrello .js
 * ================
 *
 * @Author  Ahmad Athaullah @ Qiscus 
 * @Email   <ahmad.athaullah@gmail.com>
 * @version 0.1.0
 */
/// CONFIG
var androidBoard = "5acc23711d38ba1790f89880";
var backendBoard = "5acb079b749f2ee83f37bcc8";
var frontendBoard = "5acb178091c0f71c42adac5e";
var iosBoard = "5acc3304e9c5d0c1569ea4dd";

var mapBoardClass = {};
mapBoardClass[androidBoard] = "android";
mapBoardClass[iosBoard] = "ios";
mapBoardClass[frontendBoard] = "frontend";
mapBoardClass[backendBoard] = "backend";

var colorData = [];

var boardData = {};
boardData["frontend"] = new trelloBoard();
boardData["backend"] = new trelloBoard();
boardData["android"] = new trelloBoard();
boardData["ios"] = new trelloBoard();

boardData["frontend"].member = 3.67;
boardData["frontend"].name = "Frontend";
boardData["backend"].member = 6.67;
boardData["backend"].name = "Backend";
boardData["android"].member = 3;
boardData["android"].name = "Android";
boardData["ios"].member = 3;
boardData["ios"].name = "iOS";

var mapBoardName = {};
mapBoardName[androidBoard] = "Android";
mapBoardName[iosBoard] = "iOS";
mapBoardName[frontendBoard] = "Frontend";
mapBoardName[backendBoard] = "Backend";

var mapColorDesc = [{
        color: "blue",
        desc: "SDK",
        colorhex: "#0074c2"
    },
    {
        color: "red",
        desc: "Bug/Reopen",
        colorhex: "#ff4430"
    },
    {
        color: "sky",
        desc: "Project",
        colorhex: "#00c5e4"
    },
    {
        color: "yellow",
        desc: "Throughput",
        colorhex: "#ff9836"
    },
];



var mapTeamMember = {
    "52df31b219f1119c69dca7ab": "frontend",
    "54aa3f7a17ee52c21abbca08": "frontend",
    "54fbe1640f2a1d58b17e9cd4": "backend",
    "56aed28d4439f12f6d7cd2e8": "frontend",
    "56bc0799c1a8b96058f32e7f": "android",
    "530ae8726b66ca3d37dc734e": "backend",
    "536f4d3afe4898dc6f796c9e": "backend",
    "540d9270c3922a23665c466f": "ios",
    "5426ebd31c8fafd9a34e5119": "ios",
    "5488f902852d4f60740a815d": "android",
    "5594fb1c3d25a8738100fe9d": "android",
    "5658d6990d111b01e27fae94": "backend",
    "5912c1d3a024fb9b1c44f8f6": "ios",
    "58076b055b5fb7d4d5d6b503": "frontend",
    "558861bb6734b1cb28cb46f2": "backend",
    "5a56fc92e694179ac6608e21": "backend",
    "5ae92f75360e4e7625c619c7": "backend"
};
/// ==== END CONFIG
function trelloBoard() {
    this.name = "";
    this.done = 0;
    this.notyet = 0;
    this.bug = 0;
    this.member = 0;
    this.members = [];

    this.labelColors = [];
}
trelloBoard.prototype.total = function() {
    return this.done + this.notyet;
}
trelloBoard.prototype.completion = function() {
    return (this.done / this.total()) * 100;
}
trelloBoard.prototype.score = function() {
    var comp = this.done / this.total();
    var avg = this.done / this.member;

    return (comp * avg) - (this.bug / this.member);
}
trelloBoard.prototype.best = function() {
    var bestMember;
    var bestPoint = 0;
    for (id in this.members) {
        if (this.members[id].point > bestPoint) {
            bestMember = this.members[id];
            bestPoint = this.members[id].point;
        }
    }
    return bestMember;
}

function trelloColorLabel() {
    this.done = 0;
    this.notyet = 0;
    this.labels = [];
}
trelloColorLabel.prototype.total = function() {
    return this.done + this.notyet;
}

function trelloLabel() {
    this.done = 0;
    this.notyet = 0;
    this.cardDone = [];
    this.cardNotyet = [];
}
trelloLabel.prototype.total = function() {
    return this.done + this.notyet;
}
trelloLabel.prototype.completion = function() {
    return (this.done / this.total()) * 100;
}

function trelloCard() {
    this.name = "";
    this.boardId = "";
    this.lastActivity = "";
    this.position = "";
    this.description = "";
    this.url = "";
    this.storyPoint = 0;
    this.memberId;
}
trelloCard.prototype.boardName = function() {
    return mapBoardName[this.boardId];
}
trelloCard.prototype.boardClass = function() {
    return mapBoardClass[this.boardId];
}

function trelloMember() {
    this.id = "";
    this.fullName = "";
    this.userName = "";
    this.point = 0;
    this.team = "";
    this.avatarURL = "";
    this.current = [];
}
trelloMember.prototype.score = function() {
    return this.point * (boardData[this.team].done / boardData[this.team].total());
}
// end config

var allBoards = [backendBoard, frontendBoard, androidBoard, iosBoard];

var mapListName = {};
var mapLabelPointDone = {};
var mapLabelPointNotYet = {};
var mapLabelTotal = {};
var mapLabelColor = {};
var mapListPosition = {};
var mapMemberId = {};
var mapPointMember = {};
var AllLabels = [];
var AllValues = [];
var AllMembers = [];
var AllMemberValues = [];
var AllLabelsColor = [];
var mapBoardValueDone = {};
var mapBoardValueNotYet = {};
var AllBoardName = [""];
var AllBoardValues = [0];
var mapLabelCardsDone = {};
var mapLabelCardsNotYet = {};

var sumTotal = {};


var authenticationSuccess = function() {
    console.log('Successful authentication');
    var token = Trello.token();
    $.get('https://api.trello.com/1/members/me/?key=sa&token=' + token, {}, function(data) {
        console.log(data);
        var avatarURL = data['avatarUrl'] + '/50.png';
        var fullname = data['fullName'];
        console.log(fullname);
        $("#trello-avatar").attr("src", avatarURL);
        $("#trello-fullname").html(fullname);
    });
    var loadCardsSuccess = function(successMsg) {
        if (successMsg.length > 0) {}
        // console.log("load cards", successMsg)
        $.each(successMsg, function(index, value) {
            if (value.name.includes(")") && value.name.includes("(")) {
                var mark = value.name.split("(")[1].split(")")[0];
                mark = parseInt(mark) || 0;

                // put this, so that backlog items are not being considered in the calculation
                if (mapListPosition[value.idList] == 0) {
                    return;
                }

                var boardClass = mapBoardClass[value.idBoard];

                if (mapListPosition[value.idList] >= 7) {
                    boardData[boardClass].done += mark;
                } else {
                    boardData[boardClass].notyet += mark;
                }

                // WILL BE REMOVED
                if (mapListPosition[value.idList] >= 7) {
                    if (mapBoardValueDone[value.idBoard] == undefined) {
                        mapBoardValueDone[value.idBoard] = 0;
                    }
                    mapBoardValueDone[value.idBoard] = mapBoardValueDone[value.idBoard] + mark;
                } else {
                    if (mapBoardValueNotYet[value.idBoard] == undefined) {
                        mapBoardValueNotYet[value.idBoard] = 0;
                    }
                    mapBoardValueNotYet[value.idBoard] = mapBoardValueNotYet[value.idBoard] + mark;
                }
                // -- END WILL BE REMOVED

                if (mapListPosition[value.idList] >= 7) {
                    // iterate per labels
                    $.each(value.labels, function(labelIndex, labelValue) {

                        if (labelValue.name == "BUG") {
                            boardData[boardClass].bug += mark;
                        }

                        if (boardData[boardClass].labelColors[labelValue.color] == undefined) {
                            boardData[boardClass].labelColors[labelValue.color] = new trelloColorLabel();
                        }
                        boardData[boardClass].labelColors[labelValue.color].done += mark;

                        if (colorData[labelValue.color] == undefined) {
                            colorData[labelValue.color] = new trelloColorLabel();
                        }
                        colorData[labelValue.color].done += mark;

                        if (boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name] == undefined) {
                            boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name] = new trelloLabel();
                        }
                        boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name].done += mark;

                        if (colorData[labelValue.color].labels[labelValue.name] == undefined) {
                            colorData[labelValue.color].labels[labelValue.name] = new trelloLabel();
                        }
                        colorData[labelValue.color].labels[labelValue.name].done += mark;

                        var card = new trelloCard();
                        card.name = value.name;
                        card.boardId = boardClass;
                        card.lastActivity = value.dateLastActivity;
                        card.position = mapListName[value.idList];
                        card.description = value.desc;
                        card.url = value.url;
                        card.storyPoint = mark;

                        if (value.idMembers.length > 0) {
                            if (boardData[boardClass].members[value.idMembers[0]] != undefined) {
                                card.memberId = value.idMembers[0];
                            }
                        }

                        boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name].cardDone.push(card);
                        colorData[labelValue.color].labels[labelValue.name].cardDone.push(card);

                        if (value.idMembers.length > 0) {
                            if (value.idMembers[0] in boardData[boardClass].members) {
                                boardData[boardClass].members[value.idMembers[0]].point += parseInt(mark);
                            }
                            // console.log("idMembers",value.idMembers);
                            // console.log('board',boardClass);
                        }
                        ///
                        if (mapLabelColor[labelValue.color] == undefined) {
                            mapLabelColor[labelValue.color] = [];
                        }
                        if (!mapLabelColor[labelValue.color].includes(labelValue.name)) {
                            mapLabelColor[labelValue.color].push(labelValue.name);
                        }

                        if (mapLabelPointDone[labelValue.name] == undefined) {
                            mapLabelPointDone[labelValue.name] = 0;
                        }
                        mapLabelPointDone[labelValue.name] = mapLabelPointDone[labelValue.name] + parseInt(mark);
                        if (mapLabelTotal[labelValue.name] == undefined) {
                            mapLabelTotal[labelValue.name] = 0;
                        }
                        mapLabelTotal[labelValue.name] = mapLabelTotal[labelValue.name] + parseInt(mark);
                        if (mapLabelCardsDone[labelValue.name] == undefined) {
                            mapLabelCardsDone[labelValue.name] = [];
                        }
                        mapLabelCardsDone[labelValue.name].push({
                            name: value.name,
                            board: mapBoardName[value.idBoard],
                            member: mapMemberId[value.idMembers[0]],
                            last_activity: value.dateLastActivity,
                            list: mapListName[value.idList],
                            description: value.desc,
                            url: value.url
                        });
                    })

                    // set point to members, only when the card is DONE < 
                    if (value.idMembers.length > 0) {

                        if (mapPointMember[mapMemberId[value.idMembers[0]]] == undefined) {
                            mapPointMember[mapMemberId[value.idMembers[0]]] = 0;
                        }
                        mapPointMember[mapMemberId[value.idMembers[0]]] = mapPointMember[mapMemberId[value.idMembers[0]]] + parseInt(mark);
                    }
                } else {
                    $.each(value.labels, function(labelIndex, labelValue) {

                        if (labelValue.name == "BUG") {
                            boardData[boardClass].bug += mark;
                        }

                        if (boardData[boardClass].labelColors[labelValue.color] == undefined) {
                            boardData[boardClass].labelColors[labelValue.color] = new trelloColorLabel();
                        }
                        boardData[boardClass].labelColors[labelValue.color].notyet += mark;

                        if (colorData[labelValue.color] == undefined) {
                            colorData[labelValue.color] = new trelloColorLabel();
                        }
                        colorData[labelValue.color].notyet += mark;

                        if (boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name] == undefined) {
                            boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name] = new trelloLabel();
                        }
                        boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name].notyet += mark;

                        if (colorData[labelValue.color].labels[labelValue.name] == undefined) {
                            colorData[labelValue.color].labels[labelValue.name] = new trelloLabel();
                        }
                        colorData[labelValue.color].labels[labelValue.name].notyet += mark;

                        var card = new trelloCard();
                        card.name = value.name;
                        card.boardId = boardClass;
                        card.lastActivity = value.dateLastActivity;
                        card.position = mapListName[value.idList];
                        card.description = value.desc;
                        card.url = value.url;
                        card.storyPoint = mark;

                        if (value.idMembers.length > 0) {
                            if (boardData[boardClass].members[value.idMembers[0]] != undefined) {
                                card.memberId = value.idMembers[0];
                                if (mapListName[value.idList] == "IN PROGRESS") {
                                    boardData[boardClass].members[value.idMembers[0]].current[value.id] = card;
                                }
                            }
                        }

                        boardData[boardClass].labelColors[labelValue.color].labels[labelValue.name].cardNotyet.push(card);
                        colorData[labelValue.color].labels[labelValue.name].cardNotyet.push(card);

                        // =====
                        if (mapLabelColor[labelValue.color] == undefined) {
                            mapLabelColor[labelValue.color] = [];
                        }
                        if (!mapLabelColor[labelValue.color].includes(labelValue.name)) {
                            mapLabelColor[labelValue.color].push(labelValue.name);
                        }
                        if (mapLabelPointNotYet[labelValue.name] == undefined) {
                            mapLabelPointNotYet[labelValue.name] = 0;
                        }
                        mapLabelPointNotYet[labelValue.name] = mapLabelPointNotYet[labelValue.name] + parseInt(mark);
                        if (mapLabelTotal[labelValue.name] == undefined) {
                            mapLabelTotal[labelValue.name] = 0;
                        }
                        mapLabelTotal[labelValue.name] = mapLabelTotal[labelValue.name] + parseInt(mark);
                        if (mapLabelCardsNotYet[labelValue.name] == undefined) {
                            mapLabelCardsNotYet[labelValue.name] = [];
                        }
                        mapLabelCardsNotYet[labelValue.name].push({
                            name: value.name,
                            board: mapBoardName[value.idBoard],
                            member: mapMemberId[value.idMembers[0]],
                            last_activity: value.dateLastActivity,
                            list: mapListName[value.idList],
                            description: value.desc,
                            url: value.url
                        });

                    })
                }
            }
        });
    }

    var loadCardsError = function(error) {
        console.log("error", error);
    }

    var loadListSuccess = function(successMsg) {
        $.each(successMsg, function(index, value) {
            mapListName[value.id] = value.name;
            mapListPosition[value.id] = index;
            Trello.get('/list/' + value.id + '/cards', loadCardsSuccess, loadCardsError);
        });
    };

    var loadListError = function(errorMsg) {
        console.log(errorMsg);
    };

    var loadMemberSuccess = function(successMsg) {
        $.each(successMsg, function(index, value) {
            mapMemberId[value.id] = value.username;

            if (value.id in mapTeamMember) {
                var member = new trelloMember();
                member.id = value.id;
                member.fullName = value.fullName;
                member.userName = value.username;
                member.team = mapTeamMember[value.id];

                boardData[mapTeamMember[value.id]].members[value.id] = member;
            }
        })

        $.each(allBoards, function(index, value) {
            Trello.get('/boards/' + value + '/lists', loadListSuccess, loadListError);
        })
    };

    var loadMemberFailed = function(errorMsg) {
        console.log("error load members", errorMsg);
    };


    Trello.get('/organizations/53398e2ba3b9cefc2af973b3/members', loadMemberSuccess, loadMemberFailed);

    window.setTimeout(write_result, 6000)

    function write_result() {

        var orderedmapLabelPointDone = {};
        Object.keys(mapLabelPointDone).sort().forEach(function(key) {
            orderedmapLabelPointDone[key] = mapLabelPointDone[key];
        });

        var orderedmapLabelPointNotYet = {};
        Object.keys(mapLabelPointNotYet).sort().forEach(function(key) {
            orderedmapLabelPointNotYet[key] = mapLabelPointNotYet[key];
        });

        $.each(orderedmapLabelPointDone, function(index, value) {
            sumTotal[index] = value / mapLabelTotal[index] || 0
            if (AllLabels.includes(index) == false) {
                AllLabels.push(index);
                AllValues.push(sumTotal[index])
            }
        })
        $.each(mapLabelPointNotYet, function(index, value) {
            sumTotal[index] = (mapLabelTotal[index] - value) / mapLabelTotal[index] || 0
            if (AllLabels.includes(index) == false) {
                AllLabels.push(index);
                AllValues.push(sumTotal[index])
            }

        })
        $.each(mapPointMember, function(index, value) {
            var allLeadsAndDesigner = ['yusufsyaifudin', 'ganjarwidiatmansyah', 'evan_purnama', 'sunupinasthikafajar', 'asharijuang', 'andhikayuana', 'bobyharyanto'];

            if (allLeadsAndDesigner.includes(index)) {
                return;
            }

            AllMembers.push(index);
            AllMemberValues.push(value);
        })
        $.each(allBoards, function(index, value) {
            AllBoardName.push(mapBoardName[value])
            AllBoardValues.push(getCompletionTeam(value))
        })
        window.setTimeout(loadStats, 500)
    }


};

var authenticationFailure = function() {
    document.write("please login to your Trello")
};

var getCompletionTeam = function(boardId) {
    return (mapBoardValueDone[boardId] / (mapBoardValueDone[boardId] + mapBoardValueNotYet[boardId]))
}


window.Trello.authorize({
    type: 'popup',
    name: 'Getting Started Application',
    scope: {
        read: 'true',
        write: 'true'
    },
    expiration: 'never',
    success: authenticationSuccess,
    error: authenticationFailure
});