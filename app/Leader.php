<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leader extends Model
{
	protected $primaryKey = 'id';
    protected $casts = ['id' => 'string'];
    public $incrementing = FALSE;
}
