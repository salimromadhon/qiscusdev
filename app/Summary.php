<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    protected $casts = ['id' => 'string'];
    public $incrementing = FALSE;
}
