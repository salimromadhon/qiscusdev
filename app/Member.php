<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $casts = ['id' => 'string'];
    public $incrementing = FALSE;
}
