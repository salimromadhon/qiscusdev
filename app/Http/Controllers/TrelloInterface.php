<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Trello;

use Illuminate\Http\Request;
use Session;

use App\Leader;
use App\LeaderToken;
use App\Member;
use App\Summary;

class TrelloInterface extends Trello
{	
	/*
	 * Check and bind if credential exists...
	 */
	public function authCheck(Request $request)
	{
		$key		= self::get_key();
		$username	= $request->session()->get('username');

		return view('trello.auth.in', compact('key', 'username'));
	}

	/*
	 * Try to login...
	 */
	public function authEnter(Request $request)
	{
		$trello_token	= $request->input('trelloToken');
		$trello_id		= self::username_to_id($request->session()->get('username'));

		if ($trello_token == NULL OR $trello_id == NULL) return redirect('/login');

		/*
		 * Save token first...
		 */
		$leaderToken = LeaderToken::find($trello_token);

		if ($leaderToken === NULL)
		{
			$token = new LeaderToken();
			$token->id			= $trello_token;
			$token->id_leader	= $trello_id;
			$token->save();
		}

		self::set_token($trello_token); // Push from database to object

		/*
		 * Then validate...
		 */
		if (self::auth($request))
		{
			$userData = self::api('GET', '/members/me');

			if ($userData !== NULL AND $userData->getStatusCode() == 200)
			{
				$user = json_decode($userData->getBody());

				$request->session()->put('username', $user->username);
				$request->session()->put('fullName', $user->fullName);
				$request->session()->put('avatarUrl', $user->avatarUrl);

				$request->session()->put('team', self::get_team());
			}

			return redirect('/dashboard');
		}
		else
		{
			$key		= self::get_key();
			$username	= '--invalid--';

			return view('trello.auth.in', compact('key', 'username'));
		}

		return redirect('/login');
	}

	/*
	 * Revalidate any credential directly to Trello
	 */
	public function auth(Request $request)
	{
		$remote = self::api('GET', 'tokens/'.self::get_token());

		if ($remote == NULL OR $remote->getStatusCode() != 200) return FALSE;

		$remote = json_decode($remote->getBody());

		/*
		 * Validate data...
		 */
		if (LeaderToken::where('id', self::get_token())->where('id_leader', $remote->idMember)->count() > 0 AND Leader::where('id', $remote->idMember)->count() > 0)
		{
			return TRUE;
		}

		if (self::get_token() != NULL)
		{
			$token = LeaderToken::find(self::get_token());

			if ($token !== NULL) $token->delete();
		}

		$request->session()->flush();

		return FALSE;
	}

	public function login(Request $request)
	{
		$username	= $request->input('username');
		$id			= self::username_to_id($username);

		$error = NULL;

		if ($username !== NULL AND $id !== NULL AND Leader::find($id) == NULL)
		{
			$error = 'Invalid username!';
		}
		elseif ($username !== NULL AND $id !== NULL)
		{
			$request->session()->put('username', $id);
			
			return redirect('/');
		}

		return view('trello.auth.login', compact('error'));
	}

	public function init(Request $request)
	{
		if (Leader::where('team', 'all')->count() > 0)
		{
			return redirect('/login');
		}

		$username	= $request->input('username');
		$id			= self::username_to_id($username);

		if ($username !== NULL AND $id !== NULL)
		{
			try
			{
				Leader::where('id', $id)->delete();
			}
			catch (\Illuminate\Database\QueryException $e)
			{}

			$remote = self::api('GET', 'members/'.$id);

			if ($remote !== NULL AND $remote->getStatusCode() == 200)
			{
				$leader = new Leader();
				$leader->id = $id;
				$leader->team = 'all';
				$leader->save();

				$request->session()->put('username', $id);
				
				return redirect('/');
			}
		}

		return view('trello.auth.login', compact('error'));
	}

	public function dashboard(Request $request)
	{
		$team = self::get_team();
		$time = $request->input('time');

		if ($team == 'all')
		{
			return view('trello.manager.dashboard', compact('time'));
		}
		else
		{
			return view('trello.team.dashboard', compact('team', 'time'));
		}
	}

	public function projects(Request $request)
	{
		$projectType	= 'projects';
		$projectTitle	= 'Projects';

		$team = self::get_team();
		$time = $request->input('time');

		if ($team == 'all')
		{
			return view('trello.manager.projects', compact('projectType', 'projectTitle', 'time'));
		}
		else
		{
			return view('trello.team.projects', compact('projectType', 'projectTitle', 'team', 'time'));
		}
		
	}

	public function products(Request $request)
	{
		$projectType	= 'products';
		$projectTitle	= 'Products';

		$team = self::get_team();
		$time = $request->input('time');

		if ($team == 'all')
		{
			return view('trello.manager.projects', compact('projectType', 'projectTitle', 'time'));
		}
		else
		{
			return view('trello.team.projects', compact('projectType', 'projectTitle', 'team', 'time'));
		}
		
	}

	public function team(Request $request, $team)
	{
		if (self::get_team() != 'all') return redirect('/');

		if ( ! in_array($team, ['android', 'ios', 'frontend', 'backend']))
		{
			return redirect('/');
		}

		$time = $request->input('time');

		return view('trello.manager.team', compact('team', 'time'));
	}

	public function members(Request $request, $team)
	{
		if (self::get_team() != 'all') return redirect('/');

		if ( ! in_array($team, ['android', 'ios', 'frontend', 'backend']))
		{
			return redirect('/');
		}

		$time = $request->input('time');

		return view('trello.manager.teamMembers', compact('team', 'time'));
	}

	public function teamMembers(Request $request)
	{
		if (self::get_team() == 'all') return redirect('/');

		$team = self::get_team();
		$time = $request->input('time');

		return view('trello.team.members', compact('team', 'time'));
	}

	/*
	 * Team config processor
	 */
	public function teamConf(Request $request, $team)
	{
		if (self::get_team() != 'all') return redirect('/');
		
		if ( ! in_array($team, ['android', 'ios', 'frontend', 'backend']))
		{
			return redirect('/');
		}

		$time = $request->input('time');

		$teamTitle	= '';

		switch ($team) {
			case 'android':
				$teamTitle = 'Android ';
				break;
			case 'ios':
				$teamTitle = 'iOS ';
				break;
			case 'frontend':
				$teamTitle = 'Frontend ';
				break;
			case 'backend':
				$teamTitle = 'Backend ';
				break;
			default:
				break;
		}

		$teamTitle	.= 'Developers';
		$user_ids	 = $request->input('user_id');
		$members	 = Member::where('team', $team)->get();

		if ($user_ids !== NULL)
		{
			for ($i = 0; $i < count($members); $i++)
			{				
				$member = Member::where('id', $members[$i]->id)->where('team', $team);
				
				if ($user_ids[$i] !== NULL)
				{
					try
					{
						$member->update([
							'id'		=> $user_ids[$i]
						]);
					}
					catch (\Illuminate\Database\QueryException $e)
					{}
				}
				else
				{
					try
					{
						$member->delete();
					}
					catch (\Illuminate\Database\QueryException $e)
					{}
				}
			}

			for ($i = count($members); $i < count($user_ids); $i++)
			{
				if (( ! isset($user_ids[$i])) OR $user_ids[$i] == NULL) continue;

				try
				{
					$member = new Member();
					$member->id			= $user_ids[$i];
					$member->team		= $team;
					$member->save();
				}
				catch (\Illuminate\Database\QueryException $e)
				{}
			}
		}

		$members = Member::where('team', $team)->get();

		return view('trello.manager.teamConf', compact('members', 'teamTitle', 'team', 'time'));
	}

	public function today(Request $request)
	{
		$team = self::get_team();
		$time = $request->input('time');

		$teamTitle	= '';

		switch ($team) {
			case 'android':
				$teamTitle = 'Android ';
				break;
			case 'ios':
				$teamTitle = 'iOS ';
				break;
			case 'frontend':
				$teamTitle = 'Frontend ';
				break;
			case 'backend':
				$teamTitle = 'Backend ';
				break;
			default:
				break;
		}

		if ($team == 'all')
		{
			return view('trello.manager.today', compact('time'));
		}
		else
		{
			return view('trello.team.today', compact('team', 'teamTitle', 'time'));
		}
	}

	public function configurations(Request $request)
	{
		if (self::get_team() != 'all') return redirect('/');

		/*
		 * Input variables
		 */
		$dm_ids	= $request->input('dm_id');
		$tl_ids	= $request->input('tl_id');

		/*
		 * Database data
		 */
		$managers		= Leader::where('team', 'all')->get();
		$leaders		= Leader::where('team', '!=', 'all')->get();

		/*
		 * Process Developers Managers input data...
		 */
		if ($dm_ids !== NULL)
		{
			for ($i = 0; $i < count($managers); $i++)
			{				
				$manager = Leader::where('id', $managers[$i]->id)->where('team', 'all');
				
				if ($dm_ids[$i] !== NULL)
				{
					try
					{
						$manager->update([
							'id'	=> (isset($dm_ids[$i])) ? $dm_ids[$i] : ''
						]);
					}
					catch (\Illuminate\Database\QueryException $e)
					{}
				}
				else
				{
					try
					{
						$manager->delete();
					}
					catch (\Illuminate\Database\QueryException $e)
					{}
				}
			}

			for ($i = count($managers); $i < count($dm_ids); $i++)
			{
				if (( ! isset($dm_ids[$i])) OR $dm_ids[$i] == NULL) continue;

				try
				{
					$manager = new Leader();
					$manager->id		= $dm_ids[$i];
					$manager->team		= 'all';
					$manager->save();
				}
				catch (\Illuminate\Database\QueryException $e)
				{}
			}

			$managers = Leader::where('team', 'all')->get();
		}

		/*
		 * Process Team Leaders input data...
		 */
		if ($tl_ids !== NULL)
		{
			foreach ($tl_ids as $team => $id)
			{
				$team	= strtolower($team);

				if ( ! in_array($team, ['android', 'ios', 'frontend', 'backend'])) continue;

				$leader	= Leader::where('team', $team);

				try
				{
					if ($leader->count() == 0)
					{
						$leader = new Leader;
						$leader->id		= $id;
						$leader->team	= $team;
						$leader->save();
					}
					elseif ($id == NULL)
					{
						$leader->delete();
					}
					else
					{
						$leader->update([
							'id' => $id
						]);
					}
				}
				catch (\Illuminate\Database\QueryException $e)
				{}
			}
			
			$leaders = Leader::where('team', '!=', 'all')->get();
		}

		/*
		 * Get team leaders...
		 */
		$_leaders = [
			'frontend'	=> NULL,
			'backend'	=> NULL,
			'android'	=> NULL,
			'ios'		=> NULL
		];

		foreach ($leaders as $leader) {
			$_leaders[$leader->team] = $leader;
		}

		$leaders = $_leaders;

		return view('trello.manager.configurations', compact('managers', 'leaders'));
	}

	public function logout()
	{
		$token = LeaderToken::find(self::get_token());

		if ($token !== NULL) $token->delete();

		Session::flush();

		return view('trello.auth.out');
	}

}
