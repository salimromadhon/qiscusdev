<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;

use App\LeaderToken;
use App\Leader;
use App\Member;
use App\Summary;

/*
 * (c) 2018
 * PKL Qiscus
 *
 */

class TrelloTeam {

	public $name, $url, $numDones = 0, $numNotDones = 0, $numThroughputs = 0, $numBugs = 0, $numReOpens = 0, $numMembers = 0, $idMembers = [];

}

class TrelloMember {

	public $name, $idTeams, $avatarHash, $points = [], $props = ['products' => [], 'projects' => [], 'throughputs' => 0, 'bugs' => 0, 'reOpens' => 0], $currentTasks = [];

}

class TrelloSprint {

	public $numDones = 0, $numNotDones = 0;

}

class TrelloProject {

	public $props = 0, $teamProps = ['android' => 0, 'ios' => 0, 'backend' => 0, 'frontend' => 0], $teamCompls = ['android' => 0, 'ios' => 0, 'backend' => 0, 'frontend' => 0], $remTasks = [];

}

class TrelloTask {
	
	public $name, $idTeam, $step, $url, $lastUpdate, $idMember;

}


class Trello extends Controller
{

	private $key, $token, $id, $team;

	public function __construct(Request $request)
	{
		/*
		 * Credentials
		 */
		$this->key		= 'b6f7270b2466039b679148d297cac0f8';
		$this->token	= $request->session()->get('trello_token');

		self::_set_creds();

		/*
		 * Configurations
		 */
		$this->idOrg	= '53398e2ba3b9cefc2af973b3';
		$this->idTeams	= [
			'android'	=> '5acc23711d38ba1790f89880',
			'ios'		=> '5acc3304e9c5d0c1569ea4dd',
			'backend'	=> '5acb079b749f2ee83f37bcc8',
			'frontend'	=> '5acb178091c0f71c42adac5e'
		];

		/*
		 * Prepare for output
		 */
		$this->teams	= [
			'android'	=> new TrelloTeam(),
			'ios'		=> new TrelloTeam(),
			'backend'	=> new TrelloTeam(),
			'frontend'	=> new TrelloTeam()
		];
		$this->sprints	= [];
		$this->products	= [];
		$this->projects	= [];
		$this->members	= [];

		/*
		 * Variables for mapping
		 */
		$this->map_task_step		= [];
		$this->map_member_id		= [];
		$this->map_member_team		= [];

		$members = Member::all();

		foreach ($members as $member)
		{
			if ( ! isset($this->map_member_team[$member->id]))
			{
				$this->map_member_team[$member->id] = [];
			}

			array_push($this->map_member_team[$member->id], $member->team);
		}

		$this->map_team_id			= [
			'5acc23711d38ba1790f89880' => 'android',
			'5acc3304e9c5d0c1569ea4dd' => 'ios',
			'5acb079b749f2ee83f37bcc8' => 'backend',
			'5acb178091c0f71c42adac5e' => 'frontend'
		];
		$this->map_card_type		= [
			'black'		=> 'sprint',
			'blue'		=> 'product',
			'sky'		=> 'project',
			'red'		=> 'bug',
			'yellow'	=> 'throughput',
		];
	}

	public function set_token($token)
	{
		session(['trello_token' => $token]);
		$this->token = $token;

		self::_set_creds();
	}

	private function _set_creds()
	{
		$token = LeaderToken::find($this->token);

		if ($token !== NULL)
		{
			$this->id	= $token->first()->id_leader;
			$this->team	= Leader::find($this->id);

			if ($this->team !== NULL) $this->team = $this->team->first()->team;
			else return redirect('/logout');
		}
	}

	public function get_key()
	{
		return $this->key;
	}

	public function get_token()
	{
		return $this->token;
	}

	public function get_id()
	{
		return $this->id;
	}

	public function get_team()
	{
		return $this->team;
	}

	// << Helpers <<

	public function api($method, $path, $params = NULL)
	{
		$client	= new \GuzzleHttp\Client();

		$url = 'https://api.trello.com/1/'.$path.'?key='.$this->key.'&token='.$this->token.'&'.$params;

		try
		{
			return $client->request($method, $url); 
		}
		catch (GuzzleException $e)
		{
			return $e->getResponse();
		}
	}

	public function username_to_id($username)
	{
		if ($username == NULL) return NULL;
		
		$res = self::api('GET', 'members/'.$username);

		if ($res == NULL OR $res->getStatusCode() != 200) return NULL;

		$res = json_decode($res->getBody());

		return $res->id;
	}

	public function clean_name($str)
	{
		$str	= (string) $str;

		$start	= stripos($str, ')');
		$stop	= strripos($str, '[');
		$output	= (string) '';

		for ($i = $start+1; $i < $stop; $i++) $output .= $str[$i];

		return trim($output);
	}

	public function str_max_points($str)
	{
		$str	= (string) $str;

		$start	= stripos($str, '(');
		$stop	= stripos($str, ')');
		$output	= (string) '';

		for ($i = $start+1; $i < $stop; $i++) $output .= $str[$i];
		
		return (int) $output;
	}

	public function str_points($str)
	{
		$str	= (string) $str;

		$start	= strripos($str, '[');
		$stop	= strripos($str, ']');
		$output	= (string) '';

		for ($i = $start+1; $i < $stop; $i++) $output .= $str[$i];
		
		return (int) $output;
	}

	public function get_prev_path($avoid_path)
	{
		$avoid_path = trim($avoid_path, '\/');
		$avoid_path = '/'.$avoid_path;

		$path = strtolower(parse_url(url()->previous(), PHP_URL_PATH));

		return ($path == $avoid_path) ? '/dashboard' : $path;
	}

	// >> Helpers >>

	private function _do_get_teams()
	{
		$resMembers = self::api('GET', 'organizations/'.$this->idOrg.'/members', 'fields=avatarHash,username,fullName');

		// If no connection...
		if ($resMembers == NULL OR $resMembers->getStatusCode() != 200) return NULL;

		$resMembers = json_decode($resMembers->getBody());

		foreach ($this->idTeams as $idTeam => $id)
		{
			$res = self::api('GET', 'boards/'.$id);

			if ($res == NULL OR $res->getStatusCode() != 200) return NULL;

			$res = json_decode($res->getBody());

			$team = $this->teams[$idTeam];
			$team->name	= $res->name;
			$team->url	= $res->url;

			foreach ($resMembers as $member) {

				if (isset($this->map_member_team[$member->id]))
				{
					// Make member
					$newMember				= new TrelloMember();
					$newMember->name		= $member->fullName;					
					$newMember->idTeams		= $this->map_member_team[$member->id];				
					$newMember->avatarHash	= $member->avatarHash;

					foreach ($newMember->idTeams as $newMemberIdTeam)
					{
						$newMember->points[$newMemberIdTeam] = 0;
					}

					// Save member
					$this->members[$member->username] = $newMember;

					// Map the id too
					$this->map_member_id[$member->id] = $member->username;

					// Add idMember to the team
					foreach ($newMember->idTeams as $newMemberIdTeam)
					{
						if ($newMemberIdTeam == $idTeam)
						{
							array_push($team->idMembers, $member->username);
						}
					}
				}

			}

			$team->numMembers = count($team->idMembers);

			$this->teams[$idTeam] = $team;
		}
	}

	private function _do_get_tasks_step()
	{
		foreach ($this->idTeams as $idTeam => $id) {

			$res = self::api('GET', 'boards/'.$id.'/lists', 'fields=');

			if ($res == NULL OR $res->getStatusCode() != 200) return NULL;

			$res = json_decode($res->getBody());

			for ($i = 0; $i<count($res); $i++)
			{
				$this->map_task_step[$res[$i]->id] = $i;
			}

		}
	}

	private function _do_get_projects()
	{
		foreach ($this->idTeams as $idTeam => $id)
		{

			$res = self::api('GET', 'boards/'.$id.'/cards', 'fields=idBoard,idList,name,desc,labels,idMembers,shortLink,dateLastActivity');

			if ($res == NULL OR $res->getStatusCode() != 200) return NULL;

			$res = json_decode($res->getBody());

			/*
			 * Iterate each card...
			 */
			foreach ($res as $card)
			{
				/*
				 * Record card as a task...
				 */
				$step		= $this->map_task_step[$card->idList];
				$max_points	= self::str_max_points($card->name);
				$points		= self::str_points($card->name);

				if ($step == 0 OR $max_points == 0) continue; // Skip current card if it's Backlog's card

				// Set the task...
				$task				= new TrelloTask();
				$task->name			= self::clean_name($card->name);
				$task->idTeam		= $idTeam;
				$task->step			= $step;
				$task->url			= $card->shortLink;
				$task->lastUpdate	= $card->dateLastActivity;
				$task->idMember		= (isset($card->idMembers[0]) AND isset($this->map_member_id[$card->idMembers[0]])) ? $this->map_member_id[$card->idMembers[0]] : NULL;

				// Num of done tasks and not done tasks
				if ($step >= 7)
				{
					$this->teams[$idTeam]->numDones += $max_points; // ((needToConfirm))

					// Add points to member...
					if ($task->idMember !== NULL AND in_array($idTeam, $this->members[$task->idMember]->idTeams))
					{
						$this->members[$task->idMember]->points[$idTeam] += $points;
					}					
				}
				else
				{
					$this->teams[$idTeam]->numNotDones += $max_points; // ((needToConfirm))
				}

				// Add IN PROGRESS task to member...
				if ($step == 3 AND $task->idMember !== NULL) array_push($this->members[$task->idMember]->currentTasks, $task);

				/*
				 * Iterate each label...
				 */
				foreach ($card->labels as $label)
				{
					if ( ! isset($this->map_card_type[$label->color])) continue; // Skip unneeded

					/*** Create new SPRINT, PROJECT, or PRODUCT ***/

					// SPRINT
					if (( ! isset($this->sprints[$label->name])) AND $this->map_card_type[$label->color] == 'sprint')
					{
						$this->sprints[$label->name] = new TrelloSprint();
					}

					// PRODUCT
					if (( ! isset($this->products[$label->name])) AND $this->map_card_type[$label->color] == 'product')
					{
						$this->products[$label->name] = new TrelloProject(); // Create product...
					}

					// PROJECT
					if (( ! isset($this->projects[$label->name])) AND $this->map_card_type[$label->color] == 'project')
					{
						$this->projects[$label->name] = new TrelloProject(); // Create project...
					}


					/*** Alter existing SPRINT, PROJECT, or PRODUCT ***/

					// SPRINT
					if (isset($this->sprints[$label->name]))
					{
						$sprint = $this->sprints[$label->name];
						
						if ($step < 7) $sprint->numNotDones += $max_points;
						else $sprint->numDones += $max_points;

						$this->sprints[$label->name] = $sprint; // Save sprint!
					}

					// PRODUCT
					if (isset($this->products[$label->name]))
					{
						$product = $this->products[$label->name];
						$product->props += $max_points;
						$product->teamProps[$idTeam] += $max_points;
						
						if ($step < 7) array_push($product->remTasks, $task);
						else $product->teamCompls[$idTeam] += $max_points;

						$this->products[$label->name] = $product; // Save product!
					}

					// PROJECT
					if (isset($this->projects[$label->name]))
					{
						$project = $this->projects[$label->name];
						$project->props += $max_points;
						$project->teamProps[$idTeam] += $max_points;

						if ($step < 7) array_push($project->remTasks, $task);
						else $project->teamCompls[$idTeam] += $max_points;

						$this->projects[$label->name] = $project; // Save project!
					}


					/*** Alter TEAMS data... ***/

					if ($this->map_card_type[$label->color] == 'bug')
					{
						if($label->name == 'BUG') $this->teams[$idTeam]->numBugs += $max_points; // ((needToConfirm))
						else $this->teams[$idTeam]->numReOpens += $max_points; // ((needToConfirm))
					}
					if ($this->map_card_type[$label->color] == 'throughput') $this->teams[$idTeam]->numThroughputs += $max_points; // ((needToConfirm))


					/*** Alter MEMBER data... ***/

					if ($step < 7) continue; // Skip unfinished task!

					// Bind product
					if ($this->map_card_type[$label->color] == 'product' AND isset($this->members[$task->idMember]) AND ( ! isset($this->members[$task->idMember]->props['products'][$label->name])))
					{
						$this->members[$task->idMember]->props['products'][$label->name] = 0; // Initiate product score...
					}
					// Bind project
					if ($this->map_card_type[$label->color] == 'project' AND isset($this->members[$task->idMember]) AND ( ! isset($this->members[$task->idMember]->props['projects'][$label->name])))
					{
						$this->members[$task->idMember]->props['projects'][$label->name] = 0; // Initiate project score...
					}
					// Sum product
					if (isset($this->members[$task->idMember]->props['products'][$label->name]))
					{
						$this->members[$task->idMember]->props['products'][$label->name] += $points; // ((needToConfirm))
					}
					// Sum project
					if (isset($this->members[$task->idMember]->props['projects'][$label->name]))
					{
						$this->members[$task->idMember]->props['projects'][$label->name] += $points; // ((needToConfirm))
					}
					// Sum bug/reOpen
					if ($this->map_card_type[$label->color] == 'bug' AND isset($this->members[$task->idMember]))
					{
						if($label->name == 'BUG') $this->members[$task->idMember]->props['bugs'] += $points; // ((needToConfirm))
						else $this->members[$task->idMember]->props['reOpens'] += $points; // ((needToConfirm))
					}
					// Sum troughput
					if ($this->map_card_type[$label->color] == 'throughput' AND isset($this->members[$task->idMember])) $this->members[$task->idMember]->props['throughputs'] += $points; // ((needToConfirm))
				}

			}

		}
	}

	private function _summarize()
	{
		self::_do_get_tasks_step();
		self::_do_get_teams();
		self::_do_get_projects();

		foreach ($this->idTeams as $idTeam => $id)
		{
			if ($this->teams[$idTeam]->name == NULL) return NULL;
		}

		if (
			count($this->members) == 0 OR
			count($this->products) == 0 OR
			count($this->projects) == 0
		) return NULL;
		
		$summary = [
			'teams'		=> $this->teams,
			'members'	=> $this->members,
			'sprints'	=> $this->sprints,
			'products'	=> $this->products,
			'projects'	=> $this->projects
		];

		return $summary;
	}

	private function _filter_summary($summary)
	{
		if ($summary == NULL) return NULL;

		$summary	= json_decode(json_encode($summary), TRUE);
		$team		= $this->team;

		if ($team == 'all') return $summary;

		$output		= [
			'teams'		=> [],
			'members'	=> [],
			'products'	=> [],
			'projects'	=> []
		];

		$output['teams'][$team] = $summary['teams'][$team];

		foreach ($summary['members'] as $idMember => $member)
		{
			if (in_array($team, $member['idTeams']))
			{
				$output['members'][$idMember] = $member;
			}
		}

		foreach ($summary['products'] as $idProduct => $product)
		{
			if ($product['teamProps'][$team] > 0)
			{
				foreach ($product['teamProps'] as $teamName => $value)
				{
					if ($teamName != $team) unset($product['teamProps'][$teamName]);
				}

				foreach ($product['teamCompls'] as $teamName => $value)
				{
					if ($teamName != $team) unset($product['teamCompls'][$teamName]);
				}

				foreach ($product['remTasks'] as $i => $task)
				{
					if ($task['idTeam'] != $team) unset($product['remTasks'][$i]);
				}

				$output['products'][$idProduct] = $product;
			}
		}

		foreach ($summary['projects'] as $idProject => $project)
		{
			if ($project['teamProps'][$team] > 0)
			{
				foreach ($project['teamProps'] as $teamName => $value)
				{
					if ($teamName != $team) unset($project['teamProps'][$teamName]);
				}

				foreach ($project['teamCompls'] as $teamName => $value)
				{
					if ($teamName != $team) unset($project['teamCompls'][$teamName]);
				}

				$output['projects'][$idProject] = $project;
			}
		}

		return $output;
	}

	private function _save_summary($summary, $persists = FALSE)
	{
		if ($summary == NULL) return NULL;

		$content = json_encode($summary);

		if ($persists)
		{
			$summary			= new Summary();
			$summary->id		= $this->id;
			$summary->content	= $content;
			$summary->persists	= '1';
			$summary->save();
		}
		else
		{
			$summary = Summary::find($this->id);

			if ($summary !== NULL)
			{
				$summary->where('persists', '0')->update([
					'content' => $content
				]);
			}
			else
			{
				$summary			= new Summary();
				$summary->id		= $this->id;
				$summary->content	= $content;
				$summary->persists	= '0';
				$summary->save();	
			}
		}
	}

	private function _new_summary($persists = FALSE)
	{
		$summary = self::_summarize();

		if ($summary == NULL) return NULL;

		$summary = self::_filter_summary($summary);

		self::_save_summary($summary, $persists);

		return $summary;
	}

	private function _delete_summary($time)
	{
		$summary = Summary::find($this->id);
		
		if ($summary !== NULL)
		{
			$summary = $summary->where('created_at', $time)->orWhere('updated_at', $time)->orderBy('created_at', 'desc');
			$summary->delete();
		}
	}

	public function get_summary(Request $request)
	{
		$summary = Summary::find($this->id);
		
		if ($summary !== NULL)
		{
			if ($request->has('time'))
			{
				$time = $request->input('time');
				$summary = $summary->where('created_at', $time)->orWhere('updated_at', $time)->orderBy('created_at', 'desc')->first();

				if ($summary == NULL) return NULL;
			}
			else
			{
				$summary = $summary->orderBy('updated_at', 'desc')->first();
			}

			$summary = json_decode($summary->content, TRUE);
			$summary = response($summary)->header('Content-Type', 'application/json');
		}
		else
		{
			$summary = self::_new_summary();
		}

		return $summary;
	}

	public function delete_summary(Request $request)
	{
		$summary = Summary::find($this->id);
		
		if ($summary !== NULL AND $request->has('time'))
		{
			$time = $request->input('time');

			$summary->where('persists', '1')->where('created_at', $time)->orWhere('updated_at', $time)->delete();
		}

		return redirect(self::get_prev_path('delete'));
	}

	public function new_summary(Request $request)
	{
		$summary = NULL;

		if (strtolower($request->input('persists')) == 'true')
		{
			$summary = self::_new_summary(TRUE);
		}
		else
		{
			$summary = self::_new_summary();
		}

		if ($summary == NULL) return 'Failed. Please check your configurations.';

		return redirect(self::get_prev_path('new'));
	}

	public function get_history()
	{
		$output = [];
		$summaries = Summary::find($this->id);

		if ($summaries == NULL) return NULL;

		$summaries = $summaries->get();
		
		foreach ($summaries as $summary)
		{
			$_summary = [
				'time'		=> $summary->updated_at,
				'status'	=> ($summary->persists == '1') ? 'archive' : 'latest'
			];

			array_push($output, $_summary);
		}

		return $output;
	}

	public function org()
	{
		return self::api('GET', '/organizations/'.$this->idOrg.'/members');
	}

	public function dump()
	{
		if (self::get_team() == 'all')
		{
			Summary::truncate();
		}
	}

}
