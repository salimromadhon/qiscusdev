<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\TrelloInterface;

class TrelloAuth
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$trello = new TrelloInterface($request);

		if ($trello->auth($request))
		{
			return $next($request);
		}

		return redirect('/');
	}
}
