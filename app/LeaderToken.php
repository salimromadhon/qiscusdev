<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaderToken extends Model
{
  	protected $casts = ['id' => 'string'];
  	public $incrementing = FALSE;
}
