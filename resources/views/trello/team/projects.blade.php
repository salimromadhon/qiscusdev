@extends('trello.layouts.index') @section('content')
<div class="page-title">
	<div class="title_left">
		<h3><b>{{ $projectTitle }}</b>&nbsp<small>PERFORMANCE</small></h3>
	</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>{{ $projectTitle }} List</h2>
			<ul class="nav navbar-right panel_toolbox"></ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<table id="projectList" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
		</div>
	</div>
</div>
<!-- PROJECT DETAIL -->
<div id="projectDetail" style="display: none">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h3 id="unfinishedTaskTitle">Unfinished Tasks<small>Sessions </small></h3>
				<ul class="nav navbar-right panel_toolbox"></ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="remTasksTable" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>	  
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
<script>
	// Initiate data var for projects list table...
	var data = [];
	
	// Intiate var for teams object...
	var teams = {};
	
	// Initiate var for projects object...
	var projects = {};
	
	// Initiate var for members object...
	var members = {};
	
	// Initiate var for remaining task data table...
	var remTasksTable;
	
	
	function showProject(name) {
		// Specify the project...
		var project = projects[name];
	
		var remTasksData = [];
	
		// Update team completions chart...
		// updateComplsChart(complsChartData);
		// Hide Project detail 
		$("#projectDetail").attr("style", "display: block");
		$("#unfinishedTaskTitle").html("Unfinished Task <b>" + name + "</b>");
	
		// Iterate each remaining task...
		$.each(project.remTasks, function(key, val) {
			var name = '<a href="https://trello.com/c/' + val.url + '" target="_blank">' + val.name + '</a>';
			var team = val.idTeam;
			var step = val.step;
			var idMember = val.idMember;
	
			if (members[val.idMember] != undefined) idMember = members[val.idMember].name;
	
			var lastUpdate = val.lastUpdate;
			var teamLabel = "";
			var posValue = "";
	
			switch (step) {
				case 1:
					posValue = "TODO";
	
					break;
				case 2:
					posValue = "PRIORITY";
	
					break;
				case 3:
					posValue = "IN PROGRESS";
	
					break;
				case 4:
					posValue = "DONE (DEVELOPMENT)";
	
					break;
				case 5:
					posValue = "READY TO TEST";
	
					break;
				case 6:
					posValue = "IN PROGRESS TEST";
	
					break;
				case 7:
					posValue = "DONE (QA)";
	
					break;
				case 8:
					posValue = "FINISHED";
	
					break;
				default:
					posValue = '';
	
			}
			var posValue = '<span class="label label-default">' + posValue + '</span>';
			var activity = '<span class="label label-primary">' + moment(lastUpdate).fromNow() + '</span>';
			remTasksData.push([name, posValue, idMember, activity]);
		});
	
		// Update remaining tasks table...
		remTasksTable.clear().draw();
		remTasksTable.rows.add(remTasksData); // Add new data
		remTasksTable.columns.adjust().draw(); // Redraw the DataTable
	}
	
	// Make a request to JSON data...
	var request = new XMLHttpRequest();
	request.open("GET","{{ url('/raw?time=').$time }}", false);
	request.send(null);
	
	if (request.status === 200) {
		projects = JSON.parse(request.response).{{ $projectType }};
		members = JSON.parse(request.response).members;
		teams = JSON.parse(request.response).teams;
	}
	
	$(document).ready(function() {
	
		$.each(projects, function(key, value) {
			var compls = value.teamCompls.{{ $team }};
			var props = value.teamProps.{{ $team }};
			var percentage = parseFloat(compls / props * 100).toFixed(2);
			var styleClass = "info";
	
			if (percentage < 80) styleClass = "danger";
			else if (percentage < 100) styleClass = "warning";
			else styleClass = "success";
			
			var projectTitle = '<a href="'+teams.{{ $team }}.url+'?menu=filter&filter=label:'+key+'" target="_blank">'+key+'</a>';
			var progressBar = "<div class=\"progress\"><div class=\"progress-bar progress-bar-" + styleClass + "\" style=\"width:" + percentage + "%\">" + percentage + "%</div></div>";
			var showBtn = '<a href="#teamproportionTitle" class="btn btn-primary" onclick="showProject(\'' + key + '\')" type="button">Show</a>';
	
			data.push([projectTitle, progressBar, showBtn]);
	
		});
	
		$('#projectList').DataTable({
			data: data,
			columns: [{
					title: "Name",
					width: "300"
				},
				{
					title: "Progress"
				},
				{
					title: "Action",
					width: "1"
				}
			]
		});
	
		remTasksTable = $('#remTasksTable').DataTable({
			columns: [{
					title: "Title",
					width: "350"
				},
				{
					title: "Status",
					width: "10"
				},
				{
					title: "Person",
					width: "10"
				},
				{
					title: "Activity",
					width: "10"
				}
			]
		});
	
	});
</script>
@endsection