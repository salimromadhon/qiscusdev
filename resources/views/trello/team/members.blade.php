@extends('trello.layouts.index')
@section('content')
<!-- wrapper -->
<div style="min-height: 100%">
	<div class="page-title">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><small> <b>PEOPLE PROPORTIONS</b> Sessions </small></h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table id="membersTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div id="workDetail" style="display: none">
		<!-- development proportions {{ $team }} / user  -->
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h3 id="propsTitle">Development Proportions<small></small></h3>
					<div class="clearfix"></div>
				</div>
				

				<div class="x_content">

					<div class="col-md-4 col-sm-4 col-xs-12" style="margin-right: 12%; margin-left: 4%">
						<canvas id="propsChart" height="200px"></canvas>
					</div>   
								
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12" style="height: 10px">
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="propsChartLegend" class="tile_info">
								<thead>
									<tr>
										<th style="height: 3em">Development</th>
										<th>Proportions</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>      
					</div>

				</div>

			</div>
		</div>

		<!--chart product-->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel" style="min-height: 893px">
				<div class="x_title">
					<h3 id="projectsPropsTitle">Project Proportions<small></small></h3>
					<ul class="nav navbar-right panel_toolbox">							
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<canvas id="projectsPropsChart" height="200"></canvas>
					<table id="projectsPropsTable" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
				</div>
			</div>
		</div>

		<!--chart project-->
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="x_panel" style="min-height: 893px">
				<div class="x_title">
					<h3 id="productsPropsTitle">Product Proportions<small></small></h3>
					<ul class="nav navbar-right panel_toolbox">     
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
					
				<div class="x_content">
					<canvas id="productsPropsChart" height="200"></canvas>
					<table id="productsPropsTable" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
				</div> 
			</div>
		</div>
	</div>

</div><!-- /wrapper -->

<div class="clearfix"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
<script>
	// Team
	var team = {};

	// All members
	var members = {};

	// HTML objects
	var membersTable;

	var propsChart;

	var productsPropsChart;
	var productsPropsTable;

	var projectsPropsChart;
	var projectsPropsTable;

	// Prepared variables for HTML
	var membersTableData = [];

	// Projects and products related to specific member
	var projects = [];
	var products = [];

	var propsChartData = [];

	var projectsChartLabels = [];
	var projectsChartData = [];
	var projectsChartColors = [];

	var productsChartLabels = [];
	var productsChartData = [];
	var productsChartColors = [];


	function randColors(ref) {
		var pool = 'ABCDEF0123456789';
		var output = [];

		for (x in ref) {
			var color = '#';

			for (var j=0; j<6; j++) {
				color += pool[Math.floor(Math.random() * 16)];
			}

			output.push(color);
		}

		return output;
	}

	function showDetail(name) {
		// Specify the name...
		var member = members[name];
		var memberName = name;
		var productsProps = 0;
		var projectsProps = 0;
		
		// Hide Project detail 
		$("#workDetail").attr("style", "display: block");
		$("#projectsPropsTitle").html("Projects Proportions <b>" + members[name].name + "</b>");
		$("#productsPropsTitle").html("Products Proportions <b>" + members[name].name + "</b>");
		$("#propsTitle").html("Development Proportions <b>" + members[name].name + "</b>");

		// Products
		productsChartColors = randColors(member.props.products);
		
		//var memberteam = members.{{ $team }};
		var i = 0;

		
		$.each(member.props.products, function(key, value){
			var color = productsChartColors[i];

			i++; productsProps += value;

			var boxColor = "<i class=\"fa fa-square\" style=\"color:"+color+"; margin-right: .5em\"></i>"
			var name = '<a href="'+ team.url + '?menu=filter&filter=label:'+ key +',member:'+memberName+'" target="_blank">' + boxColor + key +'</a>';
			productsChartLabels.push(key);
			productsChartData.push(value);

			products.push([name, value]);
		});

		// Projects
		projectsChartColors = randColors(member.props.projects);
		i = 0;
		$.each(member.props.projects, function(key, value){
			var color = projectsChartColors[i];

			i++; projectsProps += value;

			var boxColor = "<i class=\"fa fa-square\" style=\"color:"+color+"; margin-right: .5em\"></i>"
			var name = '<a href="'+ team.url + '?menu=filter&filter=label:'+ key +',member:'+memberName+'" target="_blank">' + boxColor + key +'</a>';

			projectsChartLabels.push(key);
			projectsChartData.push(value);

			projects.push([name, value]);
		});

		propsChartData[0] = productsProps;
		propsChartData[1] = projectsProps;
		propsChartData[2] = member.props.throughputs;
		propsChartData[3] = member.props.bugs+member.props.reOpens;

		updatePropsChart();
		updateProductProps();
		updateProjectProps();
	}

	 
	// Function updatePropsChart
	function updatePropsChart() {
		propsChart.data.datasets[0].data = propsChartData;
		propsChart.update();

		// data = [android, ios, {{ $team }}, backend]
		$("#propsChartLegend > tbody").html('');

		if (propsChartData[0] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: #1E90FF"></i> Products</td><td>' + (propsChartData[0]) + '</td></tr>');
		if (propsChartData[1] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: skyblue"></i> Projects</td><td>' + (propsChartData[1]) + '</td></tr>');
		if (propsChartData[2] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: #f0ad4e"></i> Throughputs</td><td>' + (propsChartData[2]) + '</td></tr>');
		if (propsChartData[3] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: #f65a2f"></i> Bugs/Re-opens</td><td>' + (propsChartData[3]) + '</td></tr>');

		propsChartData = [];
	}

	function updateProjectProps() {
		projectsPropsChart.data.labels = projectsChartLabels;
		projectsPropsChart.data.datasets[0].data = projectsChartData;
		projectsPropsChart.data.datasets[0].backgroundColor = projectsChartColors;
		projectsPropsChart.update();

		projectsPropsTable.clear().draw();
		projectsPropsTable.rows.add(projects); // Add new data
		projectsPropsTable.columns.adjust().draw(); // Redraw the DataTable

		projectsChartLabels = [];
		projectsChartData = [];
		projectsChartColors = [];

		projects = [];
	}

	function updateProductProps() {
		productsPropsChart.data.labels = productsChartLabels;
		productsPropsChart.data.datasets[0].data = productsChartData;
		productsPropsChart.data.datasets[0].backgroundColor = productsChartColors;
		productsPropsChart.update();

		productsPropsTable.clear().draw();
		productsPropsTable.rows.add(products); // Add new data
		productsPropsTable.columns.adjust().draw(); // Redraw the DataTable

		productsChartLabels = [];
		productsChartData = [];
		productsChartColors = [];

		products = []
	}


	// Make a request to JSON data...
	var request = new XMLHttpRequest();
	request.open("GET","{{ url('/raw?time=').$time }}", false);
	request.send(null);

	if (request.status === 200) {
		members = JSON.parse(request.response).members;

		var tempMembers = {};

		$.each(members, function (key, value) {
			if (value.idTeams.indexOf('{{ $team }}') >= 0) tempMembers[key] = value;
		});

		members = tempMembers;
		team = JSON.parse(request.response).teams.{{ $team }};

	}

	$(document).ready(function() {

		$.each(members, function (key, value) {
			value.name = '<a href="' + team.url + '?menu=filter&filter=member:' + key + '" target="_blank">' + value.name + '</a>';
			var showBtn = '<a href="#workDetail" class="btn btn-primary" onclick="showDetail(\'' + key + '\')" type="button">Show</a>';
			membersTableData.push([value.name, value.points.{{ $team }}, showBtn]);
		});

		membersTable = $('#membersTable').DataTable({
			data: membersTableData,
			columns: [
				{
					title: "Name"
				},
				{
					title: "Points",
					width: "1"
				},
				{
					title: "Action",
					width: "1"
				}
			]
		});

		productsPropsTable = $('#productsPropsTable').DataTable({
			data: [['', '']],
			columns: [
				{
					title: "Name"
				},
				{
					title: "Props",
					width: "1"
				}
			]
		});

		projectsPropsTable = $('#projectsPropsTable').DataTable({
			data: [['', '']],
			columns: [
				{
					title: "Name"
				},
				{
					title: "Props",
					width: "1"
				}
			]
		});

		// Initiate DEV PROPS chart...
		propsChart = new Chart($("#propsChart"), {
			type: 'doughnut',
			data: {
				labels: ["Products", "Projects", "Throughputs", "Bug/ReOpens"],
				datasets: [{
					label: '# of Votes',
					data: propsChartData,
					backgroundColor: [
						'#1E90FF',
						'skyblue',
						'#f0ad4e',
						'#f65a2f'
					]
				}]
			},
			options: {
				elements: {
					arc: {
						borderWidth: 1
					}
				}
			}
		});

		// Initiate PRODUCTS chart...
		productsPropsChart = new Chart($("#productsPropsChart"), {
			type: 'doughnut',
			data: {
				labels: productsChartLabels, // dynamic
				datasets: [{
					label: '',
					data: productsChartData, // dynamic
					backgroundColor: []
				}]
			},
			options: {
				elements: {
					arc: {
						borderWidth: 1
					}
				}
			}
		});

		// Initiate PROJECTS chart...
		projectsPropsChart = new Chart($("#projectsPropsChart"), {
			type: 'doughnut',
			data: {
				labels: projectsChartLabels,
				datasets: [{
					label: '',
					data: projectsChartData,
					backgroundColor: []
				}]
			},
			options: {
				elements: {
					arc: {
						borderWidth: 1
					}
				}
			}
		});

	});
</script>
@endsection