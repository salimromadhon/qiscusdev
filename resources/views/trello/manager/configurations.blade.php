@extends('trello.layouts.index') @section('content')
<div style="padding-top: 6.5%">
	
	<!-- DEV MANAGERS -->
	<div class="row top_tiles">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h3><center>Developer Managers</center></h3>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="row-fluid">
						<div class="span6">
							<form method="post">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="90%">
												<center>Trello ID <abbr title="Optional">*</abbr></center>
											</th>
											<th width="10%"></th>
										</tr>
									</thead>
									<tbody id="itemlist">
										@php
											$i = 0;
										@endphp
										@foreach ($managers as $manager)
									
										<tr>
											<td>
												<input name="dm_id[{{ $i }}]" value="{{ $manager->id }}" class="form-control">
											</td>
											<td></td>
										</tr>

										@php
											$i++;
										@endphp
										@endforeach

										<tr>
											<td>
												<input name="dm_id[{{ $i }}]" class="form-control">
											</td>
											<td></td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="1"></td>	
											<td>
												<button type="button" class="btn btn-default" onclick="additem()">+</button>
											</td>
										</tr>
									</tfoot>
								</table>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="submit" class="btn btn-success btn-sm" value="Save">
							</form>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- TEAM LEADS -->
	<div class="row top_tiles">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h3><center>Team Leaders</center></h3>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<div class="row-fluid">
						<div class="span6">
							<form method="post">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th width="80%">
												<center>Trello ID <abbr title="Optional">*</abbr></center>
											</th>
											<th width="20%">
												<center>Team <abbr title="Required">*</abbr></center>
											</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($leaders as $team => $leader)
										@php
											$id		= (isset($leader->id)) ? $leader->id : NULL;
										@endphp
										<tr>
											<td>
												<input name="tl_id[{{ $team }}]" value="{{ $id }}" class="form-control">
											</td>
											<td>
												<input class="form-control" disabled="" value="{{ $team }}">
											</td>
										</tr>
										@endforeach
									</tbody>
									<tfoot></tfoot>
								</table>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="submit" class="btn btn-success btn-sm" value="Save">
							</form>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- TEAM MEMBERS -->
	<div class="row top_tiles">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h3><center>Team Members</center></h3>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-12">
				<center>
					<a class="btn btn-primary btn-outline btn-lg" href="configurations/backend">Backend</a>
					<a class="btn btn-danger btn-outline btn-lg" href="configurations/frontend">Frontend</a>
					<a class="btn btn-success btn-outline btn-lg" href="configurations/android">Android</a>
					<a class="btn btn-warning btn-outline btn-lg" href="configurations/ios">iOS</a>
				</center>
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

</div>

<script>
	var i = {{ $i+1 }};

	function additem() {
		//               
		var itemlist = document.getElementById('itemlist');

		//              
		var row = document.createElement('tr');
		var colA = document.createElement('td');
		var colB = document.createElement('td');

		//                
		itemlist.appendChild(row);
	    row.appendChild(colA);
		row.appendChild(colB);

		//                
		var dm_id = document.createElement('input');
		dm_id.setAttribute('name', 'dm_id[' + i + ']');
		dm_id.setAttribute('class', 'form-control');

		var deleteBtn = document.createElement('span');

		//
		colA.appendChild(dm_id);
		colB.appendChild(deleteBtn);

		deleteBtn.innerHTML = '<button type="button" class="btn btn-default">×</button>';
		//                
		deleteBtn.onclick = function() {
			row.parentNode.removeChild(row);
		};

		i++;
	}
</script>

@endsection