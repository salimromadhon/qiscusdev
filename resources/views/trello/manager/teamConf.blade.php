@extends('trello.layouts.index') @section('content')
<div style="height: 1808px">
<!-- top tiles -->
<div class="col-md-12 col-sm-12 col-xs-12" style="height: 37px">
	<a href="{{ url('/configurations') }}"><button type="button" class="btn btn btn-default btn-sm  btn-md"><b>Back</b></button></a>
</div>

<div class="row top_tiles">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h3><center>{{ $teamTitle }}</center></h3>
				<ul class="nav navbar-right panel_toolbox">

				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row-fluid">
					<div class="span6">
						<form method="post">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th width="100%">
											<center>Trello ID <abbr title="Required">*</abbr></center>
										</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="itemlist">
								@php
									$i = 0;
								@endphp

								@foreach ($members as $member)

									<tr>
										<td>
											<input value='{{ $member->id }}' name="user_id[{{ $i }}]" class="form-control">
										</td>

										<td></td>
									</tr>

								@php
									$i++;
								@endphp
								@endforeach

								<tr>
									<td>
										<input name="user_id[{{ $i }}]" class="form-control">
									</td>
								</tr>

								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td>

											<button type="button" class="btn btn-default" onclick="additem()">+</button>

										</td>
									</tr>
								</tfoot>
							</table>
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="submit" class="btn btn-success btn-sm" value="Save">
						</form>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12" style="height: 165px">

</div>
</div>

<script>
	var i = {{ $i+1 }};

	function additem() {
		//               
		var itemlist = document.getElementById('itemlist');

		//              
		var row = document.createElement('tr');
		var colA = document.createElement('td');
		var colB = document.createElement('td');

		//                
		itemlist.appendChild(row);
		row.appendChild(colA);
		row.appendChild(colB);

		//                
		var user_id = document.createElement('input');
		user_id.setAttribute('name', 'user_id[' + i + ']');
		user_id.setAttribute('class', 'form-control');

		var deleteBtn = document.createElement('span');

		//               
		colA.appendChild(user_id);
		colB.appendChild(deleteBtn);

		deleteBtn.innerHTML = '<button type="button" class="btn btn-default">×</button>';
		//                
		deleteBtn.onclick = function() {
			row.parentNode.removeChild(row);
		};

		i++;
	}
</script>

@endsection