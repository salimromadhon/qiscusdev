@extends('trello.layouts.index') @section('content')
<div class="page-title">
	<div class="title_left">
		<h3><b>{{ $projectTitle }}</b>&nbsp<small>PERFORMANCE</small></h3>
	</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>{{ $projectTitle }} List</h2>
			<ul class="nav navbar-right panel_toolbox"></ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<table id="projectList" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"></table>
		</div>
	</div>
</div>
<!-- PROJECT DETAIL -->
<div id="projectDetail" style="display: none">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel" style="width:100%; min-height: 5px">
			<div class="x_title">
				<h3 id="teamproportionTitle">Team Proportions<small>Sessions</small></h3>
				<ul class="nav navbar-right panel_toolbox">

					
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
			
				<div class="col-md-4 col-sm-4 col-xs-12" style="margin-left: 10px; margin-right: 50px; min-height: 100px">

					 <!-- <canvas id="propsChart" height="600px" width="300px"></canvas> /-> -->
					<canvas id="propsChart" height="300"></canvas>

				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="col-md-12 col-sm-12 col-xs-12">
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12" >
						<table id="propsChartLegend" class="tile_info">
							 <div id="demo" class="collapse in">
							<thead>
								<tr>
									<th>Team</th>
									<th>Proportions</th>
								</tr>
							</thead>

							<tbody></tbody>
						</table>
					</div>
				</div>

			</div>

		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel" style="width:100%;">
			<div class="x_title">
				<h3 id="teamcompletionTitle">Team Completions<small>Sessions </small></h3>
				<ul class="nav navbar-right panel_toolbox">

					
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" style="margin-top: 20px">
				<table id="teamComplsTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<th>Team</th>
						<th>Progress</th>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h3 id="unfinishedTaskTitle">Unfinished Tasks<small>Sessions </small></h3>
				<ul class="nav navbar-right panel_toolbox"></ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="remTasksTable" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>	  
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
<script>
	// Initiate data var for projects list table...
	var data = [];
	
	// Intiate var for teams object...
	var teams = {};
	
	// Initiate var for projects object...
	var projects = {};
	
	// Initiate var for members object...
	var members = {};
	
	// Initiate var for remaining task data table...
	var remTasksTable;
	
	//variabel propsChart...
	var propsChart;
	var propsLabel;
	
	
	function showProject(name) {
		// Specify the project...
		var project = projects[name];
	
		// Android, iOS, Frontend, Backend
		var propsChartData = [
			project.teamProps.android,
			project.teamProps.ios,
			project.teamProps.frontend,
			project.teamProps.backend
		];
	
		var propsLabelData = [
			project.teamProps.android,
			project.teamProps.ios,
			project.teamProps.frontend,
			project.teamProps.backend
		];
	
		// Update team proportions chart...
		updatePropsChart(propsChartData);
	
	
		var remTasksData = [];
		// Android, iOS, Frontend, Backend
	
		// Update team completions chart...
		// updateComplsChart(complsChartData);
		// Hide Project detail 
		$("#projectDetail").attr("style", "display: block");
	
	
		$("#teamproportionTitle").html("Team Proportions <b>" + name + "</b>");
		$("#unfinishedTaskTitle").html("Unfinished Task <b>" + name + "</b>");
		$("#teamcompletionTitle").html("Team Completion <b>" + name + "</b>");
	
		// Iterate each remaining task...
		$.each(project.remTasks, function(key, val) {
			var name = '<a href="https://trello.com/c/' + val.url + '" target="_blank">' + val.name + '</a>';
			var team = val.idTeam;
			var step = val.step;
			var idMember = val.idMember;
	
			if (members[val.idMember] != undefined) idMember = members[val.idMember].name;
	
			var lastUpdate = val.lastUpdate;
			var teamLabel = "";
	
			switch (team) {
				case "frontend":
					teamLabel = '<span class="label label-info">Frontend</span>';
					break;
				case "backend":
					teamLabel = '<span class="label label-danger">Backend</span>';
					break;
				case "android":
					teamLabel = '<span class="label label-success">Android</span>';
					break;
				case "ios":
					teamLabel = '<span class="label label-warning">iOS</span>';
					break;
				default:
					teamLabel = '';
			}
	
			var posValue = "";
	
			switch (step) {
				case 1:
					posValue = "TODO";
	
					break;
				case 2:
					posValue = "PRIORITY";
	
					break;
				case 3:
					posValue = "IN PROGRESS";
	
					break;
				case 4:
					posValue = "DONE (DEVELOPMENT)";
	
					break;
				case 5:
					posValue = "READY TO TEST";
	
					break;
				case 6:
					posValue = "IN PROGRESS TEST";
	
					break;
				case 7:
					posValue = "DONE (QA)";
	
					break;
				case 8:
					posValue = "FINISHED";
	
					break;
				default:
					posValue = '';
	
			}
			var posValue = '<span class="label label-default">' + posValue + '</span>';
			var activity = '<span class="label label-primary">' + moment(lastUpdate).fromNow() + '</span>';
			remTasksData.push([name, teamLabel, posValue, idMember, activity]);
		});
	
		// Update remaining tasks table...
		remTasksTable.clear().draw();
		remTasksTable.rows.add(remTasksData); // Add new data
		remTasksTable.columns.adjust().draw(); // Redraw the DataTable
	
		// Update team completion table...
		var androidCompls	= project.teamCompls.android / project.teamProps.android;
		var iosCompls		= project.teamCompls.ios / project.teamProps.ios;
		var frontendCompls	= project.teamCompls.frontend / project.teamProps.frontend;
		var backendCompls	= project.teamCompls.backend / project.teamProps.backend;
	
		androidCompls	= parseFloat(androidCompls * 100).toFixed(2);
		iosCompls		= parseFloat(iosCompls * 100).toFixed(2);
		frontendCompls	= parseFloat(frontendCompls * 100).toFixed(2);
		backendCompls	= parseFloat(backendCompls * 100).toFixed(2);
		
	
		// clear code (don't duplicate code)
		$("#teamComplsTable > tbody").html('');
	
		// Make a progress bar...
		function progressBar(progress) {
			var styleClass;
			if (progress < 80) styleClass = "danger";
			else if (progress < 100) styleClass = "warning";
			else styleClass = "success";
	
			return "<div class=\"progress\"><div class=\"progress-bar progress-bar-" + styleClass + "\" style=\"width:" + progress + "%\">"+progress+"%</div></div>";
		}
		
		if (project.teamProps.android > 0) 	$("#teamComplsTable > tbody").append('<tr><td><a href="'+teams.android.url+'?menu=filter&filter=label:'+name+'" target="_blank">Android</a></td><td>'+progressBar(androidCompls)+'</td></tr>');
		if (project.teamProps.ios > 0)		$("#teamComplsTable > tbody").append('<tr><td><a href="'+teams.ios.url+'?menu=filter&filter=label:'+name+'"target="_blank">iOS</a></td><td>'+progressBar(iosCompls)+'</td></tr>');
		if (project.teamProps.frontend > 0)	$("#teamComplsTable > tbody").append('<tr><td><a href="'+teams.frontend.url+'?menu=filter&filter=label:'+name+'"target="_blank">Frontend</a></td><td>'+progressBar(frontendCompls)+'</td></tr>');
		if (project.teamProps.backend > 0)	$("#teamComplsTable > tbody").append('<tr><td><a href="'+teams.backend.url+'?menu=filter&filter=label:'+name+'"target="_blank">Backend</a></td><td>'+ progressBar(backendCompls)+'</td></tr>');	
	
		// Update proportions chart
		function updatePropsChart(data) {
			propsChart.data.datasets[0].data = data;
			propsChart.update();
		
			var totalProps = data[0] + data[1] + data[2] + data[3];
		
			// data = [android, ios, frontend, backend]
			$("#propsChartLegend > tbody").html('');
		
			if (data[0] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: #26b99a"></i><a href="'+teams.android.url+'?menu=filter&filter=label:'+name+'"target="_blank">Android</a></td><td>' + parseFloat(data[0] / totalProps * 100).toFixed(2) + '%</td></tr>');
			if (data[1] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: #f0ad4e"></i><a href="'+teams.ios.url+'?menu=filter&filter=label:'+name+'"target="_blank">iOS</a></td><td>' + parseFloat(data[1] / totalProps * 100).toFixed(2) + '%</td></tr>');
			if (data[2] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: #3498db"></i><a href="'+teams.frontend.url+'?menu=filter&filter=label:'+name+'"target="_blank">Frontend</a></td><td>' + parseFloat(data[2] / totalProps * 100).toFixed(2) + '%</td></tr>');
			if (data[3] > 0) $("#propsChartLegend > tbody").append('<tr><td><i class="fa fa-square" style="color: #d9534f"></i><a href="'+teams.backend.url+'?menu=filter&filter=label:'+name+'"target="_blank">Backend</a></td><td>' + parseFloat(data[3] / totalProps * 100).toFixed(2) + '%</td></tr>');
		}
	}
	
	// Make a request to JSON data...
	var request = new XMLHttpRequest();
	request.open("GET","{{ url('/raw?time=').$time }}", false);
	request.send(null);
	
	if (request.status === 200) {
		projects = JSON.parse(request.response).{{ $projectType }};
		members = JSON.parse(request.response).members;
		teams = JSON.parse(request.response).teams;
	}
	
	$(document).ready(function() {
	
		$.each(projects, function(key, value) {
			var compls = value.teamCompls.android +
				value.teamCompls.ios +
				value.teamCompls.backend +
				value.teamCompls.frontend;
			var props = value.teamProps.android +
				value.teamProps.ios +
				value.teamProps.backend +
				value.teamProps.frontend;
			var percentage = parseFloat(compls / props * 100).toFixed(2);
			var styleClass = "info";
	
			if (percentage < 80) styleClass = "danger";
			else if (percentage < 100) styleClass = "warning";
			else styleClass = "success";
	
			var progressBar = "<div class=\"progress\"><div class=\"progress-bar progress-bar-" + styleClass + "\" style=\"width:" + percentage + "%\">" + percentage + "%</div></div>";
	
			var showBtn = '<a href="#teamproportionTitle" class="btn btn-primary" onclick="showProject(\'' + key + '\')" type="button">Show</a>';
	
			data.push([key, progressBar, showBtn]);
	
		});
	
		$('#projectList').DataTable({
			data: data,
			columns: [{
					title: "Name",
					width: "300"
				},
				{
					title: "Progress"
				},
				{
					title: "Action",
					width: "1"
				}
			]
		});
	
		// Initiate team proportions chart...
		propsChart = new Chart($("#propsChart"), {
			type: 'doughnut',
			data: {
				labels: ["Android", "iOS", "Frontend", "Backend"],
				datasets: [{
					label: '# of Votes',
					data: [0, 0, 0, 0],
					backgroundColor: [
						'#26b99a',
						'#f0ad4e',
						'#3498db',
						'#d9534f'
					]
				}]
			},
			options: {
				elements: {
					arc: {
						borderWidth: 1
					}
				}
			}
		});
	
		remTasksTable = $('#remTasksTable').DataTable({
			columns: [{
					title: "Title",
					width: "350"
				},
				{
					title: "Team",
					width: "10"
				},
				{
					title: "Status",
					width: "10"
				},
				{
					title: "Person",
					width: "10"
				},
				{
					title: "Activity",
					width: "10"
				}
			]
		});
	
	});
</script>
@endsection