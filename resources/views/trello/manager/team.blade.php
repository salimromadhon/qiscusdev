@extends('trello.layouts.index') 
@section('content')

<!-- wrapper -->
<div style="min-height: 100%">
	<!-- page content -->
	<div class="col-md-12 col-sm-12 col-xs-12" style="min-height: 37px">
		
		<center>
		<a href="/teams/{{ $team }}/members"><button type="button" class="btn btn btn-default btn-sm  btn-md teamButtonLeft" style="font-size: 12px;"><b><i class="fa fa-group"></i>&nbsp See Members</b></button></a>
		</center>

	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Development<small>Proportions</small></h2>
				<ul class="nav navbar-right panel_toolbox">     
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">

				<div class="col-md-4 col-sm-4 col-xs-12" style="margin-right: 12%; margin-left: 4%">
					<canvas id="ChartDevelopment" height="300px"></canvas>
				</div>
											
				<div class="col-md-6 col-sm-6 col-xs-12">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<table class="tile_info" >
							<thead>
								<tr>
									<th style="height: 3em">Development</th>
									<th>Proportions</th>
								</tr>
							</thead>   
							<tbody>
								<tr>
									<td>
										<p><i class="fa fa-square" style="color: #1E90FF"></i>Products</p>
									</td>
									<td id="ProductsProps">0</td>
								</tr>
								<tr>
									<td>
										<p><i class="fa fa-square" style="color: skyblue"></i>Projects</p>
									</td>
									<td id="ProjectsProps">0</td>
								</tr>
								<tr>
									<td>
										<p><i class="fa fa-square" style="color: #f0ad4e"></i>Throughput</p>
									</td>
									<td id="Throughput">0</td>
								</tr>
								<tr>
									<td>
										<p><i class="fa fa-square" style="color: #f65a2f"></i>Bug/Reopen</p>
									</td>
									<td id="BugReopen">0</td>
								</tr>
											
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel" style="min-height: 893px">
			<div class="x_title">
				<h2>Products<small>Proportions</small></h2>
				<ul class="nav navbar-right panel_toolbox">     									
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>		
			<div class="x_content">
				<canvas id="ChartProducts" height="200"></canvas>
					<table id="productsList" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel" style="min-height: 893px">
			<div class="x_title">
				<h2>Projects<small>Proportions</small></h2>
				<ul class="nav navbar-right panel_toolbox">     								
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>				
			<div class="x_content">
				<canvas id="ChartProjects" height="200"></canvas>
				<table id="projectsList" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
			</div>  
		</div>
	</div>
</div><!-- /wrapper -->

<div class="clearfix"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	var result = {};

	var team = null;

	// Initiate data var for products list table...
	var dataProducts = [];

	// Initiate data var for projects list table...
	var dataProjects = [];

	// Var Data Products and Projects
	var dataChartProducts = [];
	var colorChartProducts = [];
	var nameChartProducts = [];
	var dataChartProductsProps = [];
	var dataChartProjects = [];
	var colorChartProjects = [];
	var nameChartProjects = [];
	var dataChartProjectsProps = [];
	var totalProportionProjects = 0;
	var totalProportionProducts = 0;
	var throughput = 0;

	function randColors(ref) {
		var pool = 'ABCDEF0123456789';
		var output = [];

		for (x in ref) {
			var color = '#';

			for (var j = 0; j < 6; j++) {
				color += pool[Math.floor(Math.random() * 16)];
			}

			output.push(color);
		}

		return output;
	}

	// Make a request to JSON data...
	var request = new XMLHttpRequest();
	request.open("GET","{{ url('/raw?time=').$time }}", false);
	request.send(null);

	if (request.status === 200) {
		result	= JSON.parse(request.response);
		team	= result.teams.{{ $team }};

		var products = result.products;
		var projects = result.projects;

		var tempProducts = {};
		var tempProjects = {};

		$.each(products, function (key, value) {
			if (value.teamProps.{{ $team }} > 0) tempProducts[key] = value;
		});

		$.each(projects, function (key, value) {
			if (value.teamProps.{{ $team }} > 0) tempProjects[key] = value;
		});

		result.products = tempProducts;
		result.projects = tempProjects;


		// Get total team Bug/Reopen and Throughput
		var totalBugReopen = team.numBugs + team.numReOpens;
		var totalThroughput = team.numThroughputs;

		// Generate colors
		var colorChartProducts = randColors(result.products);
		var colorChartProjects = randColors(result.projects);
	}

	$(document).ready(function() {

		$.each(result.products, function(key, value) {
			totalProportionProducts += value.teamProps.{{ $team }};
		});

		$.each(result.projects, function(key, value) {
			totalProportionProjects += value.teamProps.{{ $team }};
		});

		// Products and Projects
		var i = 0;
		$.each(result.products, function(key, value) {
			var color = colorChartProducts[i]; i++;

			var boxColor = "<i class=\"fa fa-square\" style=\"color:" + color + " \"></i> "

			var name = '<a href="'+ team.url + '?menu=filter&filter=label:'+ key +'" target="_blank">' + boxColor + key +'</a>';
			var percents = parseFloat(value.teamProps.{{ $team }}	/ totalProportionProducts * 100).toFixed(2);
			var props = value.teamProps.{{ $team }};

			dataProducts.push([name, props, percents]);

			dataChartProducts.push([
				percents
			]);

			dataChartProductsProps.push([
				props
			]);

			nameChartProducts.push([
				key
			]);
		});

		i = 0;
		$.each(result.projects, function(key, value) {
			var color = colorChartProjects[i]; i++;

			var boxColor = "<i class=\"fa fa-square\" style=\"color:" + color + " \"></i> "
			
			var name = '<a href="'+ team.url + '?menu=filter&filter=label:'+ key +'" target="_blank">' + boxColor + key +'</a>';
			var percents = parseFloat(value.teamProps.{{ $team }} / totalProportionProjects * 100).toFixed(2);
			var props = value.teamProps.{{ $team }};
		
			dataProjects.push([name, props, percents]);

			dataChartProjectsProps.push([
				props
			]);

			dataChartProjects.push([
				percents
			]);

			nameChartProjects.push([
				key
			]);
		});

		//Tabel Products
		$('#productsList').DataTable({
			data: dataProducts,
			columns: [{
				title: "Products",
				width: "50%"
			}, {
				title: "Props",
				width: "20%"
			}, {
				title: "Percents (%)",
				width: "30%"
			}]
		});

		//Tabel Projects
		$('#projectsList').DataTable({
			data: dataProjects,
			columns: [{
				title: "Project",
				width: "50%"
			}, {
				title: "Props",
				width: "20%"
			}, {
				title: "Percents (%)",
				width: "30%"
			}]
		});

		var data = {
			datasets: [{
				data: dataChartProductsProps,
				backgroundColor: colorChartProducts,
				label: 'My dataset' // for legend
			}],
			labels: nameChartProducts
		};

		var ChartProducts = new Chart($('#ChartProducts'), {
			data: data,
			type: 'doughnut',
			options: {
				elements: {
					arc: {
						borderWidth: 1
					}
				}
			}
		});

		var data = {
			datasets: [{
				data: dataChartProjectsProps,
				backgroundColor: colorChartProjects,
				label: 'My dataset' // for legend
			}],
			labels: nameChartProjects
		};

		var ChartProjects = new Chart($('#ChartProjects'), {
			data: data,
			type: 'doughnut',
			options: {
				elements: {
					arc: {
						borderWidth: 1
					}
				}
			}
		});


		// Development Percentage
		var totalDevelopment = totalThroughput + totalBugReopen + totalProportionProjects + totalProportionProducts;


		$("#Throughput").html(totalThroughput); $("#BugReopen").html(totalBugReopen); $("#ProductsProps").html(totalProportionProducts); $("#ProjectsProps").html(totalProportionProjects);


		// Development Chart
		var data = {
			datasets: [{
				data: [totalProportionProducts, totalProportionProjects, totalThroughput, totalBugReopen],
				backgroundColor: [
					'#1E90FF',
					'skyblue',
					'#f0ad4e',
					'#f65a2f'
				],
				label: 'My dataset' // for legend
			}],
			labels: [
				"Products",
				"Projects",
				"Throughput",
				"Bug/Reopen"
			]
		};

		var ChartProducts = new Chart($('#ChartDevelopment'), {
			data: data,
			type: 'doughnut',
			options: {
				elements: {
					arc: {
						borderWidth: 1
					}
				}
			}
		});

	});
</script>

@endsection


