@extends('trello.layouts.index') 
@section('content')
<!-- page content -->
<!-- top tiles -->
<div class="row top_tiles">
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats">
			<div class="icon"><i class="fa fa-html5" style="color: #1E90FF"></i></div>
			<div id="frontend-score" class="count">0</div>
			<h3>Frontend</h3>
		</div>
	</div>
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats">
			<div class="icon"><i class="fa fa-gear" style="color: #f65a2f"></i></div>
			<div id="backend-score" class="count" >0</div>
			<h3>Backend</h3>
		</div>
	</div>
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats">
			<div class="icon"><i class="fa fa-android" style="color: #50C878"></i></div>
			<div id="android-score" class="count">0</div>
			<h3>Android</h3>
		</div>
	</div>
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="tile-stats">
			<div class="icon"><i class="fa fa-apple" style="color: #f0ad4e"></i></div>
			<div id="ios-score" class="count">0</div>
			<h3>iOS</h3>
		</div>
	</div>
</div>
<!-- /top tiles -->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="dashboard_graph">
			<div class="row x_title">
				<div class="col-md-6">
					<h3>Overall Development Report</h3>
				</div>
			</div>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<canvas id="ChartSprints"></canvas>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12 bg-white">
				<div class="x_title">
					<h4>Team Completion</h4>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-3">
					<div>
						<div class="row">
							<div class="col-sm-9">
								<p>Frontend Team Completion</p>
							</div>
							<div class="col-sm-3">
								<p id="frontend-comp">(0/0)</p>
							</div>
						</div>
						<div class="">
							<div class="progress progress_sm" style="width: 100%;">
								<div id="frontend-prog" class="progress-bar bg-green" role="progressbar" style=""></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-3">
					<div>
						<div class="row">
							<div class="col-sm-9">
								<p>Backend Team Completion</p>
							</div>
							<div class="col-sm-3">
								<p id="backend-comp">(0/0)</p>
							</div>
						</div>
						<div class="">
							<div class="progress progress_sm" style="width: 100%;">
								<div id="backend-prog" class="progress-bar bg-green" role="progressbar" style=""></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-3">
					<div>
						<div class="row">
							<div class="col-sm-9">
								<p>Android Team Completion</p>
							</div>
							<div class="col-sm-3">
								<p id="android-comp">(0/0)</p>
							</div>
						</div>
						<div class="">
							<div class="progress progress_sm" style="width: 100%;">
								<div id="android-prog" class="progress-bar bg-green" role="progressbar" style=""></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-3">
					<div>
						<div class="row">
							<div class="col-sm-9">
								<p>IOS Team Completion</p>
							</div>
							<div class="col-sm-3">
								<p id="ios-comp">(0/0)</p>
							</div>
						</div>
						<div class="">
							<div class="progress progress_sm" style="width: 100%;">
								<div id="ios-prog" class="progress-bar bg-green" role="progressbar" style=""></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<table class="table table-striped">
						<tbody>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h4><b id="frontend-percen">0%</b></h4>
									</center>
								</td>
							</tr>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h5>FRONTEND COMPLETION</h5>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<table  class="table table-striped">
						<tbody>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h4><b id="backend-percen">0%</b></h4>
									</center>
								</td>
							</tr>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h5>BACKEND COMPLETION</h5>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<table  class="table table-striped">
						<tbody>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h4><b id="android-percen">0%</b></h4>
									</center>
								</td>
							</tr>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h5>ANDROID COMPLETION</h5>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<table class="table table-striped">
						<tbody>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h4><b id="ios-percen">0%</b></h4>
									</center>
								</td>
							</tr>
							<tr>
								<td style="padding: 0px;">
									<center>
										<h5>IOS COMPLETION</h5>
									</center>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<br />
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Development<small>Proportions</small></h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<div class="col-md-4 col-sm-4 col-xs-12" style="margin-right: 12%; margin-left: 4%">
				<canvas id="ChartDevelopment" height="300px"></canvas>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12" style="height: 85px">
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<table class="tile_info" >
						<thead>
								<tr>
									<th style="height: 3em">Development</th>
									<th>Proportions</th>
								</tr>
							</thead>   
						<tbody>
							<tr>
								<td>
									<p><i class="fa fa-square" style="color: #1E90FF"></i>Products</p>
								</td>
								<td id="percenProductsProps">0%</td>
							</tr>
							<tr>
								<td>
									<p><i class="fa fa-square" style="color: skyblue"></i>Projects</p>
								</td>
								<td id="percenProjectsProps">0%</td>
							</tr>
							<tr>
								<td>
									<p><i class="fa fa-square" style="color: #f0ad4e"></i>Throughput</p>
								</td>
								<td id="percenThroughput">0%</td>
							</tr>
							<tr>
								<td>
									<p><i class="fa fa-square" style="color: #f65a2f"></i>Bug/Reopen</p>
								</td>
								<td id="percenBugReopen">0%</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Products<small>Proportion</small></h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<canvas id="ChartProducts" height="200"></canvas>
			<table id="productsList" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
		</div>
	</div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h2>Projects<small>Proportions</small></h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<canvas id="ChartProjects" height="200"></canvas>
			<table id="projectsList" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
		</div>
	</div>
</div>

<div class="clearfix"></div>

<div class="best-developers">
	<div class="col-md-6 col-sm-6 col-xs-12 profile_details">
		<div class="well profile_view" style="width: 100%">
			<div class="col-sm-12">
				<h4 class="brief">BEST FRONTEND DEVELOPER</h4>
				<div class="right col-xs-5 text-center">
					<div id="frontend-bestImg" class="img-circle-best"></div>
				</div>
				<div class="left col-xs-7">
					<h4 id="frontend-bestName" align="center">
					----
					<h4>
					<h2 align="center"><strong id="frontend-bestScore">0</strong></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 profile_details">
		<div class="well profile_view" style="width: 100%">
			<div class="col-sm-12">
				<h4 class="brief">BEST BACKEND DEVELOPER</h4>
				<div class="right col-xs-5 text-center">
					<div id="backend-bestImg" class="img-circle-best"></div>
				</div>
				<div class="left col-xs-7">
					<h4 id="backend-bestName" align="center">
					----
					<h4>
					<h2 align="center"><strong id="backend-bestScore">0</strong></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 profile_details">
		<div class="well profile_view" style="width: 100%">
			<div class="col-sm-12">
				<h4 class="brief">BEST ANDROID DEVELOPER</h4>
				<div class="right col-xs-5 text-center">
					<div id="android-bestImg" class="img-circle-best"></div>
				</div>
				<div class="left col-xs-7">
					<h4 align="center" id="android-bestName">
					----
					<h4>
					<h2 align="center"><strong id="android-bestScore">0</strong></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 profile_details">
		<div class="well profile_view" style="width: 100%">
			<div class="col-sm-12">
				<h4 class="brief">BEST IOS DEVELOPER</h4>
				<div class="right col-xs-5 text-center">
					<div id="ios-bestImg" class="img-circle-best"></div>
				</div>
				<div class="left col-xs-7">
					<h4 id="ios-bestName" align="center">
					----
					<h4>
					<h2 align="center"><strong id="ios-bestScore">0</strong></h2>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	// Initiate data var for products list table...
	var dataProducts = [];

	// Initiate data var for projects list table...
	var dataProjects = [];

	function TrelloMember() {
		this.name = "";
		this.points = 0;
		this.avatarHash = "";
	}

	var bestMembers = {};
	bestMembers["android"] = new TrelloMember();
	bestMembers["ios"] = new TrelloMember();
	bestMembers["frontend"] = new TrelloMember();
	bestMembers["backend"] = new TrelloMember()

	function randColors(ref) {
		var output = [];

		for (x in ref) {
			output.push(randomColor());
		}

		return output;
	}

	$(document).ready(function() {

		$.getJSON("{{ url('/raw?time=').$time }}", function(result) {
			$.each(result.teams, function(key, value) {

				var total = value.numDones + value.numNotDones;
				var comp = value.numDones / total;
				var avg = value.numDones / value.numMembers;
				var score = comp * avg - value.numBugs / value.numMembers;

				var teamID = "#" + key + "-score";

				// Write score...
				$(teamID).html(parseFloat(score).toFixed(2));

				var percentage = parseFloat(comp * 100).toFixed(2);

				// Write progress bar...
				var progID = "#" + key + "-prog";

				$(progID).attr("style", "width: " + percentage + "%");

				// Write completion in x/y
				var compID = "#" + key + "-comp";
				$(compID).html("(" + value.numDones + "/" + total + ")");


				// Write completion in percentage...
				var compID = "#" + key + "-percen";
				$(compID).html(percentage + "%");

			});

			$.each(result.members, function(key, value) {
				$.each(value.points, function(k, v) {
					if (bestMembers[k].points < value.points[k]) {
						bestMembers[k].name = value.name;
						bestMembers[k].points = value.points[k];
						bestMembers[k].avatarHash = value.avatarHash;
					}
				});
			});

			console.log(bestMembers);

			$.each(bestMembers, function(key, value) {
				var bestImgID = "#" + key + "-bestImg";
				$(bestImgID).attr("style", "background: url('https://trello-avatars.s3.amazonaws.com/" + bestMembers[key].avatarHash + "/170.png') no-repeat; background-size: cover; height: 120px; width: 120px;");

				var bestNameID = "#" + key + "-bestName";
				$(bestNameID).html(bestMembers[key].name);

				var bestScoreID = "#" + key + "-bestScore";
				$(bestScoreID).html(bestMembers[key].points);
			});


			// Get total products and projects proportions

			var totalProductsProps = 0;
			var totalProjectsProps = 0;

			$.each(result.products, function(key, value) {
				totalProductsProps += value.props;
			});

			$.each(result.projects, function(key, value) {
				totalProjectsProps += value.props;
			});

			// Get total team Bug/Reopen and Throughput

			var totalBugReopen = 0;
			var totalThroughput = 0;

			$.each(result.teams, function(key, value) {
				totalThroughput += value.numThroughputs;
				totalBugReopen += value.numBugs + value.numReOpens;
			});


			// Var Data Products and Projects
			var dataChartSprintsTotal = [];
			var dataChartSprintsDone = [];
			var nameChartSprints = [];
			var dataChartProducts = [];
			var colorChartProducts = randColors(result.products);
			var nameChartProducts = [];
			var dataChartProjects = [];
			var colorChartProjects = randColors(result.projects);
			var nameChartProjects = [];

			var sprints = {};
			Object.keys(result.sprints).sort().forEach(function(key) {
				sprints[key] = result.sprints[key];
			});

			$.each(sprints, function(key, value) {
				nameChartSprints.push(key);
				dataChartSprintsDone.push(value.numDones);
				dataChartSprintsTotal.push(value.numDones + value.numNotDones);
			});

			// Products
			var i = 0;
			$.each(result.products, function(key, value) {

				var percentageProducts = parseFloat(value.props / totalProductsProps * 100).toFixed(2);

				var percenProducts = percentageProducts + "%";

				var color = colorChartProducts[i];
				i++;

				var boxColor = "<i class=\"fa fa-square\" style=\"color:" + color + " \"></i> "

				var nameProducts = boxColor + key;

				dataProducts.push([nameProducts, value.props, percenProducts]);

				dataChartProducts.push([
					value.props
				]);

				nameChartProducts.push([
					key
				]);

				console.log(color);
			});

			// Projects
			i = 0;
			$.each(result.projects, function(key, value) {

				var percentageProjects = parseFloat(value.props / totalProjectsProps * 100).toFixed(2);

				var percenProjects = percentageProjects + "%";

				//random color
				var color = colorChartProjects[i];
				i++;


				var boxColor = "<i class=\"fa fa-square\" style=\"color:" + color + " \"></i> "
				var nameProjects = boxColor + key;

				dataProjects.push([nameProjects, value.props, percenProjects]);

				dataChartProjects.push([
					value.props
				]);

				nameChartProjects.push([
					key
				]);

			});


			// Development Percentage
			var totalDevelopment = totalThroughput + totalBugReopen + totalProductsProps + totalProjectsProps;

			var percenThroughput = parseFloat(totalThroughput / totalDevelopment * 100).toFixed(2);
			var percenBugReopen = parseFloat(totalBugReopen / totalDevelopment * 100).toFixed(2);
			var percenProductsProps = parseFloat(totalProductsProps / totalDevelopment * 100).toFixed(2);
			var percenProjectsProps = parseFloat(totalProjectsProps / totalDevelopment * 100).toFixed(2);

			$("#percenThroughput").html(percenThroughput + "%");
			$("#percenBugReopen").html(percenBugReopen + "%");
			$("#percenProductsProps").html(percenProductsProps + "%");
			$("#percenProjectsProps").html(percenProjectsProps + "%");


			// Products Table
			$('#productsList').DataTable({
				data: dataProducts,
				columns: [{
						title: "Products",
						width: "80%"
					},
					{
						title: "Props",
						width: "10%"
					},
					{
						title: "%",
						width: "10%"
					}
				]

			});

			// Projects Table
			$('#projectsList').DataTable({
				data: dataProjects,
				columns: [{
						title: "Projects",
						width: "80%"
					},
					{
						title: "Props",
						width: "10%"
					},
					{
						title: "%",
						width: "10%"
					}
				]

			});

			// Chart Sprints
			var ChartSprints = new Chart($('#ChartSprints'), {
				data: {
					datasets: [{
							data: dataChartSprintsDone,
							backgroundColor: 'rgb(75,148,191,.6)',
						},
						{
							data: dataChartSprintsTotal,
							backgroundColor: 'rgb(156,194,203,.6)',
						}
					],
					labels: nameChartSprints
				},
				type: 'line',
				options: {
					elements: {
						arc: {
							borderWidth: 1
						}
					}
				}
			});

			// Chart Development
			var data = {
				datasets: [{
					data: [totalProductsProps, totalProjectsProps, totalThroughput, totalBugReopen],
					backgroundColor: [
						"#1E90FF",
						"skyblue",
						"#f0ad4e",
						"#f65a2f"
					],
					label: 'My dataset' // for legend
				}],
				labels: [
					"Products",
					"Projects",
					"Throughput",
					"Bug/Reopen"
				]
			};

			var ChartProducts = new Chart($('#ChartDevelopment'), {
				data: data,
				type: 'doughnut',
				options: {
					elements: {
						arc: {
							borderWidth: 1
						}
					}
				}
			});

			// Chart Products
			var ChartProducts = new Chart($('#ChartProducts'), {
				data: {
					datasets: [{
						data: dataChartProducts,
						backgroundColor: colorChartProducts,
					}],
					labels: nameChartProducts
				},
				type: 'doughnut',
				options: {
					elements: {
						arc: {
							borderWidth: 1
						}
					}
				}
			});

			// ChartProjects
			var ChartProjects = new Chart($('#ChartProjects'), {
				data: {
					datasets: [{
						data: dataChartProjects,
						backgroundColor: colorChartProjects,
					}],
					labels: nameChartProjects
				},
				type: 'doughnut',
				options: {
					elements: {
						arc: {
							borderWidth: 1
						}
					}
				}
			});
		});
	});
</script>
@endsection