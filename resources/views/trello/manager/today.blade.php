@extends('trello.layouts.index') 
@section('content')
<div class="page-title">
	<div class="title_left">
		<h3><b>TODAY</b>&nbsp<small>TASK</small></h3>
	</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h3>Frontend</h3>
			<ul class="nav navbar-right panel_toolbox">			
			</ul>
			<div class="clearfix"></div>
		</div>
		<div id="frontend"></div>
	</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h3>Backend</h3>
			<ul class="nav navbar-right panel_toolbox">			
			</ul>
			<div class="clearfix"></div>
		</div>
		<div id="backend"></div>
	</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h3>Android</h3>
			<ul class="nav navbar-right panel_toolbox">			
			</ul>
			<div class="clearfix"></div>
		</div>
		<div id="android"></div>
	</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
		<div class="x_title">
			<h3>iOS</h3>
			<ul class="nav navbar-right panel_toolbox">			
			</ul>
			<div class="clearfix"></div>
		</div>
		<div id="ios"></div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
<!-- This is a copy of palette.js -->
<script src="https://codepen.io/anon/pen/aWapBE.js"></script>
<script>
	var members = {};

	function todayMember(idTeam, name, avatarHash, currentTasks) {
		var output = '';
		var styleClass;
		var currentTaskNull;

		switch (idTeam) {
			case 'frontend':
				styleClass = 'info';
				currentTaskNull = '<span class="label label-info">Free</span>';
				break;
			case 'backend':
				styleClass = 'danger';
				currentTaskNull = '<span class="label label-danger">Free</span>';
				break;
			case 'android':
				styleClass = 'success';
				currentTaskNull = '<span class="label label-success">Free</span>';
				break;
			case 'ios':
				styleClass = 'warning';
				currentTaskNull = '<span class="label label-warning">Free</span>';
				break;
		}

		output += '<div class="x_content">';
		output += '<article class="media event">';
		output += '<div style="width: 100px; height: 100px; background: grey url(\'https://trello-avatars.s3.amazonaws.com/'+avatarHash+'/170.png\') no-repeat; background-size: cover; margin-right: .5em; border-radius: 5px" class="pull-left"></div>';
		output += '<div class="media-body">';
		output += '<div class="panel panel-'+styleClass+'">';
		output += '<div class="panel-heading">';
		output += '<b>'+name+'</b>';
		output += '</div>';
		output += '<div class="panel-body">';

		if (currentTasks.length > 0) output += '<ul style="padding-left:1em">';
		$.each(currentTasks, function (key, val) {
			output += '<li><a href="https://trello.com/c/'+val.url+'" target="_blank">'+val.name+'</a></li>';
		});
		if (currentTasks.length > 0) output += '</ul>';

		if (currentTasks.length == 0) {
			output += '<p><strong>'+currentTaskNull+'</strong></p>';
		}
		
		output += '</div>';
		output += '</div>';
		output += '</div>';
		output += '</article>';
		output += '</div>';

		return output;
	}

	// Make a request to JSON data...
	var request = new XMLHttpRequest();
	request.open("GET","{{ url('/raw?time=').$time }}", false);
	request.send(null);

	if (request.status === 200) {
		members = JSON.parse(request.response).members;
	}

	$(document).ready(function() {
		$.each(members, function(key, val){
			$.each(val.idTeams, function(k, v) {
				$('#'+v).append(todayMember(v, val.name, val.avatarHash, val.currentTasks));
			});
		});
	});
</script>

@endsection