	<!-- top navigation -->
	<div class="top_nav">
		<div class="nav_menu">
			<nav>
				<div class="nav toggle">
					<a id="menu_toggle"><i class="fa fa-bars"></i></a>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li>						
						<a href="" data-toggle="dropdown" aria-expanded="false">
							<span class="profile_info1">
								<h2><span class="fa fa-user"></span> &nbsp<span class="fa fa-angle-down"></span></h2>
							</span>
						</a>
						<ul class="dropdown-menu dropdown-usermenu pull-right">
							<li><a href="" data-toggle="modal" data-target="#historyModal">Archives</a></li>
							<li><a href="" data-toggle="modal" data-target="#logoutModal"><i class="fa fa-sign-out pull-right"></i> Logout</a></li>
						</ul>
					</li>
					<li>
						<a id="active-history" data-toggle="modal" data-target="#historyModal" style="background: transparent; cursor: pointer;"></a>
					</li>
				</ul>
			</nav>
		</div>
	</div>

	<!-- ModalLogout-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Do you want to logout?</h4>
				</div>
				<div class="modal-body">
					You will lost all of your unsaved changes.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a type="button" class="btn btn-success" href="{{ url('/logout') }}">Logout</a>
				</div>
			</div>
		</div>
	</div>
	<!-- ModalHistory -->
	<div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<center><h4 class="modal-title" id="myModalLabel">Archives</h4></center>
				</div>
				<div class="modal-body">
					<div class="alert alert-info alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  Here you can just update the existing data by clicking <strong>Update</strong> or update and archive the latest data by clicking <strong>Update &amp; Archive</strong>.
					</div>
					<div style="margin-bottom: 10px; text-align: center; border-bottom: 1px dotted #ddd; padding-bottom: 1em">
						<a class="btn btn-primary btn-outline btn-lg" href="{{ url('/new') }}">Update</a>
						<a class="btn btn-success btn-outline btn-lg" href="{{ url('/new?persists=true') }}">Update &amp Archive</a>
						<div class="clearfix"></div>
					</div>
					<table id="historyTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">			
				</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>			
				</div>
			</div>
		</div>
	</div>


