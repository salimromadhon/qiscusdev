<!-- sidebar menu -->
		
		@if (session('team') == 'all')
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<ul class="nav side-menu">
					<li><a class="timeRelated" href="{{ url('/dashboard') }}"><i class="fa fa-pie-chart"></i> Overall </span></a></li>
					<li><a class="timeRelated" href="{{ url('/projects') }}"><i class="fa fa-suitcase"></i> Projects</span></a></li>
					<li><a class="timeRelated" href="{{ url('/products') }}"><i class="fa fa-gavel"></i> Products</span></a></li>
					<li><a><i class="fa fa-users"></i> Teams<span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a class="timeRelated" href="{{ url('/teams/frontend') }}"><i class="fa fa-html5"></i>Front-end</a></li>
							<li><a class="timeRelated" href="{{ url('/teams/backend') }}"><i class="fa fa-gear"></i>Back-end</a></li>
							<li><a class="timeRelated" href="{{ url('/teams/android') }}"><i class="fa fa-android"></i>Android</a></li>
							<li><a class="timeRelated" href="{{ url('/teams/ios') }}"><i class="fa fa-apple"></i>iOS</a></li>
						</ul>
					</li>
					<li><a class="timeRelated" href="{{ url('/today') }}"><i class="fa fa-calendar"></i>Today</span></a></li>
					<li><a class="timeRelated" href="{{ url('/configurations') }}"><i class="fa fa-cogs"></i>Configurations</span></a></li>
				</ul>
			</div>
		</div>

		@else

		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<ul class="nav side-menu">
				   <li><a class="timeRelated" href="{{ url('/dashboard') }}"><i class="fa fa-pie-chart"></i> Overall </span></a></li>
				   <li><a class="timeRelated" href="{{ url('/projects') }}"><i class="fa fa-suitcase"></i> Projects</span></a></li>
				   <li><a class="timeRelated" href="{{ url('/products') }}"><i class="fa fa-gavel"></i> Products</span></a></li>
				   <li><a class="timeRelated" href="{{ url('/members') }}"><i class="fa fa-group"></i> Members</span></a></li>
				   <li><a class="timeRelated" href="{{ url('/today') }}"><i class="fa fa-calendar"></i>Today</span></a></li>
				</ul>
			</div>
		</div>

		@endif

	</div>
</div>

