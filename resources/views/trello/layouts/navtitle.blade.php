<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0;">
			<a href="/dashboard" class="site_title"> <img src="{{ asset('/assets/images/qiscuslogo1.png') }}" alt="..." class="img-rounded"> <span><strong>Qiscus</strong>Dev</span></a>
		</div>

		<div class="clearfix"></div>

		<!-- menu profile quick info -->
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="{{ session('avatarUrl') }}/170.png" alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info" style="margin-top: .5em">
				<h2>{{ session('fullName') }}</h2>
				<span><span>@</span>{{ session('username') }}</span>
			</div>
		</div>
		<!-- /menu profile quick info -->