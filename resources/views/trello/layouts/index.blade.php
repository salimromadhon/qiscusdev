<!DOCTYPE html>
<html lang="en">

<head>
	@include('trello.layouts.head')
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('trello.layouts.navtitle') 
			@include('trello.layouts.sidebar') 
			@include('trello.layouts.topnavigation')
			<div class="right_col" role="main">
				@yield('content') @include('trello.layouts.footercontent')
			</div>
		</div>
	</div>
	@include('trello.layouts.footer')
</body>

</html>