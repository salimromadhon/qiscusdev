	<!-- jQuery -->
	<script src="{{ asset('/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
	<!-- Bootstrap -->
	<script src="{{ asset('/assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<!-- FastClick -->
	<script src="{{ asset('/assets/vendors/fastclick/lib/fastclick.js') }}"></script>
	<!-- NProgress -->
	<script src="{{ asset('/assets/vendors/nprogress/nprogress.js') }}"></script>
	<!-- Chart.js -->
	<script src="{{ asset('/assets/vendors/Chart.js/dist/Chart.min.js') }}"></script>
	<!-- randomColor.js -->
	<script src="{{ asset('/assets/vendors/davidmerfield/randomColor.min.js') }}"></script>
	<!-- morris.js -->
	<script src="{{ asset('/assets/vendors/raphael/raphael.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/morris.js/morris.min.js') }}"></script>
	<!-- gauge.js -->
	<script src="{{ asset('/assets/vendors/gauge.js/dist/gauge.min.js') }}"></script>
	<!-- bootstrap-progressbar -->
	<script src="{{ asset('/assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
	<!-- iCheck -->
	<script src="{{ asset('/assets/vendors/iCheck/icheck.min.js') }}"></script>
	<!-- Skycons -->
	<script src="{{ asset('/assets/vendors/skycons/skycons.js') }}"></script>
	<!-- Flot -->
	<script src="{{ asset('/assets/vendors/Flot/jquery.flot.js') }}"></script>
	<script src="{{ asset('/assets/vendors/Flot/jquery.flot.pie.js') }}"></script>
	<script src="{{ asset('/assets/vendors/Flot/jquery.flot.time.js') }}"></script>
	<script src="{{ asset('/assets/vendors/Flot/jquery.flot.stack.js') }}"></script>
	<script src="{{ asset('/assets/vendors/Flot/jquery.flot.resize.js') }}"></script>
	<!-- Flot plugins -->
	<script src="{{ asset('/assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
	<script src="{{ asset('/assets/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/flot.curvedlines/curvedLines.js') }}"></script>
	<!-- DateJS -->
	<script src="{{ asset('/assets/vendors/DateJS/build/date.js') }}"></script>
	<!-- JQVMap -->
	<script src="{{ asset('/assets/vendors/jqvmap/dist/jquery.vmap.js') }}"></script>
	<script src="{{ asset('/assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
	<script src="{{ asset('/assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="{{ asset('/assets/vendors/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	<!-- Datatables -->
	<script src="{{ asset('/assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
	<script src="{{ asset('/assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/jszip/dist/jszip.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>
	<!-- Custom Theme Scripts -->
	<script src="{{ asset('/assets/js/custom.js') }}"></script>
	<!--modalLogout&History-->
	<script>
		$('#logoutModal').on('shown.bs.modal', function () {
		  $('#myInput').focus()
		})
		$('#historyModal').on('shown.bs.modal', function () {
		  $('#myInput1').focus()
		})

		var latestData;
		var history;
		var historyData = [];

		function readDate(d)
		{
			d = new Date(d);
			var monthName = [
			    'January',
			    'February',
			    'March',
			    'April',
			    'May',
			    'June',
			    'July',
			    'August',
			    'September',
			    'October',
			    'November',
			    'December'
			];

			function leadingZero(i)
			{
				i = i.toString();
				if (i.length < 2) return '0'+i;
				return i;
			}

			return d.getDate() + " " + monthName[d.getMonth()] + " " + leadingZero(d.getFullYear()) + " " + leadingZero(d.getHours()) + ":" + leadingZero(d.getMinutes()) + ":" + leadingZero(d.getSeconds());
		}

		// Make a request to JSON data...
		var request = new XMLHttpRequest();
		request.open("GET","{{ url('/history') }}", false);
		request.send(null);

		if (request.status === 200) {
			histories = JSON.parse(request.response);
		}

		$(document).ready(function() {
			$.each(histories, function(key, val) {
				if (val.status == 'latest') {
					latestData = val;
					return;
				}

				var niceDate	= readDate(val.time.date);
				var viewLink	= '<a class="text-info" href="?time='+val.time.date+'">View</a>';
				var deleteLink	= '<a class="text-danger" href="{{ url('/delete') }}?time='+val.time.date+'">Delete</a>';

				// Check whether the date is active and make it bold...
				if (niceDate == readDate(getParameterByName('time')))
				{
					niceDate += ' &middot; <strong class="text-success">Active</strong>';
				}

				historyData.push([niceDate, viewLink+' &middot; '+deleteLink]);
			});

			$('#historyTable').DataTable({
				data: historyData,
				columns: [
					{
						title: "Date",
						width: "300"
					},
					{
						title: "Action",
						width: "1"
					}
				]
			});

			// For history query string
			function getParameterByName(name) {
			    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
			    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
			}

			var persistedQueryParam = getParameterByName('time');

			if (persistedQueryParam && persistedQueryParam.length > 0) {
			  $('a[href].timeRelated').each(function () {
			    var elem = $(this);
			    var href = elem.attr('href');
			    elem.attr('href', href + (href.indexOf('?') != -1 ? '&' : '?') + 'time=' + persistedQueryParam);
			  });
			}

			if (getParameterByName('time') != null && getParameterByName('time') != '') {
				$("#active-history").html('<span class="text-danger"><strong>Archive</strong> &middot; '+readDate(getParameterByName('time'))+'</span>');
			} else {
				$("#active-history").html('<span class="text-success"><strong>Latest</strong> &middot; '+readDate(latestData.time.date)+'</span>');
			}

		});
	</script>



	    