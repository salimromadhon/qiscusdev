<!DOCTYPE html>
<html lang="en">
<head>
	<title>QiscusDev</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" href="{{ asset('/assets/images/qiscuslogo.png') }}" type="image/png">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/animate/animate.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/animsition/css/animsition.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/select2/select2.min.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/daterangepicker/daterangepicker.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/main.css') }}">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form class="login100-form validate-form" action="" method="POST">
					
					<img src="{{ asset('/assets/images/qiscus4.png') }}" class="responsive">

					<div class="wrap-input100 validate-input" data-validate = "App-key is required!">

						<input name="username" type="text" placeholder="Username on Trello" class="input100">
    					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</div>

					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn" onclick="_token">
							LOGIN
						</button>
					</div>

					<div class="text-center p-t-45 p-b-4">
						<span class="txt1">
							Crafted in 2018 &middot; PKL Komsi
						</span>
					</div>
					
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('/assets/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/daterangepicker/moment.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/js/main.js') }}"></script>

</body>
</html>