<html>
<head>
	<title>QiscusDev</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" href="{{ asset('/assets/images/qiscuslogo.png') }}" type="image/png">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/animate/animate.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/animsition/css/animsition.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/select2/select2.min.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/vendors/daterangepicker/daterangepicker.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/animate.css') }}">
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	@if ($username != NULL)
		<script src="https://trello.com/1/client.js?key={{ $key }}"></script>
	@endif
	
	<style type="text/css">
		.login100-form-btn {
			text-decoration: none;
			box-shadow: 0 3px 20px 0px rgba(0, 0, 0, 0.1);
			background: white;
			color: #71c1b8;
		}
	</style>
</head>
<body>
	<div class="limiter">
	<div class="container-login100">
	
        <div class="col-md-12 col-sm-12 col-xs-12" style="height:35%">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" style="height:65%; text-align:center">
               	 <div><i class="fa fa-spinner w3-spin" style="font-size:70px;color:white"></i></div>
		         <div style="margin: 2em auto; color: white; font-weight: bold">Please wait&hellip;</div>
		         <div><a class="login100-form-btn" href="{{ url('/login') }}">Or, retry &rarr;</a></div>
        </div>
    </div>
</div>
 

    <script src="{{ asset('/assets/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('/assets/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/daterangepicker/moment.min.js') }}"></script>
	<script src="{{ asset('/assets/vendors/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/vendors/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('/assets/js/main.js') }}"></script>
	<script>
		function post(path, params, method) {
			method = method || "post"; // Set method to post by default if not specified.

			// The rest of this code assumes you are not using a library.
			// It can be made less wordy if you use one.
			var form = document.createElement("form");
			form.setAttribute("method", method);
			form.setAttribute("action", path);

			for(var key in params) {
				if(params.hasOwnProperty(key)) {
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", key);
					hiddenField.setAttribute("value", params[key]);

					form.appendChild(hiddenField);
				}
			}

			document.body.appendChild(form);
			form.submit();
		}

		$(document).ready(function () {

			@if ($username == NULL)

				if (localStorage.getItem('trello_token') == undefined) {
					window.location = "{{ url('/login') }}";
				} else {
					post("{{ url('/auth/enter') }}", {trelloToken: localStorage.getItem('trello_token'), _token: "{{ csrf_token() }}"});
					console.log("init: "+localStorage.getItem('trello_token'));
				}

			@elseif ($username == '--invalid--')

				if (localStorage.getItem('trello_token') != undefined) {
					localStorage.removeItem('trello_token');
					console.log("remove existing trello_token");
				}

				window.location = "{{ url('/login') }}";

			@else

				var authSuccess = function () {
					post("{{ url('/auth/enter') }}", {trelloToken: localStorage.getItem('trello_token'), _token: "{{ csrf_token() }}"});
					console.log("succ: "+localStorage.getItem('trello_token'));
				}

				var authFail = function () {
					window.location.replace('{{ url('/login') }}');
					console.log("fail: "+localStorage.getItem('trello_token'));
				}

				Trello.authorize({
					type: 'popup',
					name: 'QiscusDev',
					persist: true,
					interactive: true,
					expiration: '1day',
					success: authSuccess,
					error: authFail
				});

			@endif

		});
	</script>

</body>

</html>
