<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<script>
		localStorage.removeItem('trello_token');
		window.location = "{{ url('/login') }}";
	</script>
</body>
</html>