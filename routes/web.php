<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/init', 'TrelloInterface@init');
Route::post('/init', 'TrelloInterface@init');

Route::get('/login', 'TrelloInterface@login');
Route::post('/login', 'TrelloInterface@login');

Route::get('/', 'TrelloInterface@authCheck');
Route::post('/auth/set', 'TrelloInterface@authSet');
Route::post('/auth/enter', 'TrelloInterface@authEnter');

Route::get('/logout', 'TrelloInterface@logout');

Route::group(['middleware' => ['trelloAuth']], function () {

	Route::get('/dashboard', 'TrelloInterface@dashboard');

	Route::get('/projects', 'TrelloInterface@projects');
	Route::get('/products', 'TrelloInterface@products');

	Route::get('/teams/{team}', 'TrelloInterface@team');
	Route::get('/teams/{team}/members', 'TrelloInterface@members');
	Route::get('/members', 'TrelloInterface@teamMembers');

	Route::get('/today', 'TrelloInterface@today');

	Route::get('/raw', 'Trello@get_summary');
	Route::get('/new', 'Trello@new_summary');
	Route::get('/delete', 'Trello@delete_summary');

	Route::get('/history', 'Trello@get_history');

	Route::get('/configurations', 'TrelloInterface@configurations');
	Route::post('/configurations', 'TrelloInterface@configurations');

	Route::get('/configurations/{team}', 'TrelloInterface@teamConf');
	Route::post('/configurations/{team}', 'TrelloInterface@teamConf');
	
	Route::get('/org', 'Trello@org');

	Route::get('/dump', 'Trello@dump');
});